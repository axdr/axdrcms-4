/*
Navicat MySQL Data Transfer

Source Server         : phpmyadmin
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : plus

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2018-06-29 20:33:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bans
-- ----------------------------
DROP TABLE IF EXISTS `bans`;
CREATE TABLE `bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bantype` enum('user','ip','machine') NOT NULL DEFAULT 'user',
  `value` varchar(50) NOT NULL,
  `reason` text NOT NULL,
  `expire` double NOT NULL DEFAULT '0',
  `added_by` varchar(50) NOT NULL,
  `added_date` varchar(50) NOT NULL,
  `appeal_state` enum('0','1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `value` (`value`) USING BTREE,
  KEY `bantype` (`bantype`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bans
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(125) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT 'defaultuser@meth0d.org',
  `auth_ticket` varchar(60) DEFAULT NULL,
  `rank` int(1) unsigned DEFAULT '1',
  `task` varchar(100) NOT NULL,
  `rank_vip` int(1) DEFAULT '0',
  `credits` int(11) DEFAULT '50000',
  `seasonal_currency` int(11) NOT NULL DEFAULT '0',
  `country_cf` varchar(100) NOT NULL,
  `vip_points` int(11) DEFAULT '0',
  `activity_points` int(11) DEFAULT '5000',
  `look` char(255) DEFAULT NULL,
  `gender` enum('M','F') DEFAULT 'M',
  `motto` char(50) DEFAULT NULL,
  `account_created` char(12) DEFAULT '0',
  `last_online` int(11) DEFAULT '0',
  `online` enum('0','1') DEFAULT '0',
  `ip_last` varchar(45) DEFAULT '',
  `ip_reg` varchar(45) DEFAULT NULL,
  `ip_current` varchar(45) DEFAULT '',
  `home_room` int(10) DEFAULT '0',
  `is_muted` enum('0','1') DEFAULT '0',
  `block_newfriends` enum('0','1') DEFAULT '0',
  `hide_online` enum('0','1') DEFAULT '0',
  `hide_inroom` enum('0','1') DEFAULT '0',
  `vip` enum('0','1') DEFAULT '0',
  `volume` varchar(15) DEFAULT '100,100,100',
  `last_change` int(20) DEFAULT '0',
  `machine_id` varchar(125) DEFAULT '',
  `focus_preference` enum('0','1') DEFAULT '0',
  `chat_preference` enum('0','1') DEFAULT '0',
  `pets_muted` enum('0','1') DEFAULT '0',
  `bots_muted` enum('0','1') DEFAULT '0',
  `advertising_report_blocked` enum('0','1') DEFAULT '0',
  `gotw_points` int(11) DEFAULT '0',
  `prefix_name` varchar(100) NOT NULL,
  `prefix_color` varchar(100) NOT NULL,
  `name_color` varchar(100) NOT NULL,
  `ignore_invites` enum('0','1') DEFAULT '0',
  `time_muted` double DEFAULT '0',
  `allow_gifts` enum('0','1') DEFAULT '1',
  `trading_locked` double DEFAULT '0',
  `friend_bar_state` enum('0','1') NOT NULL DEFAULT '1',
  `disable_forced_effects` enum('0','1') NOT NULL DEFAULT '0',
  `allow_mimic` enum('1','0') NOT NULL DEFAULT '1',
  `rangovandeweek` enum('1','0') CHARACTER SET utf8 DEFAULT '0',
  `staff_sinds` text,
  `rank_lijst` text CHARACTER SET utf8,
  `bcrypt_on` enum('1','0') CHARACTER SET utf8 DEFAULT '1',
  `rank_name` text CHARACTER SET utf8,
  `team` int(10) DEFAULT NULL,
  `staff_mail` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `rankup` mediumtext CHARACTER SET utf8,
  `rank_section` int(11) DEFAULT '0',
  `rank_section_level` int(11) DEFAULT '0',
  `talentstatus` enum('citizenship',' helper') DEFAULT NULL,
  `facebook_id` int(20) NOT NULL,
  `can_receive_gifts` enum('0','1') NOT NULL DEFAULT '0',
  `can_receive_trade` enum('0','1') NOT NULL DEFAULT '0',
  `can_receive_follow` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING HASH,
  UNIQUE KEY `username` (`username`) USING BTREE,
  KEY `rank` (`rank`),
  KEY `ip_last` (`ip_last`),
  KEY `ip_reg` (`ip_reg`),
  KEY `credits` (`credits`),
  KEY `activity_points` (`activity_points`),
  KEY `online` (`online`),
  KEY `mail` (`mail`),
  KEY `machine_id` (`machine_id`),
  KEY `auth_ticket` (`auth_ticket`),
  KEY `last_online` (`last_online`),
  KEY `home_room` (`home_room`),
  KEY `messenger` (`id`,`username`,`look`,`motto`,`last_online`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Mr.Mustache', null, 'defaultuser@meth0d.org', null, '13', '', '0', '79', '0', '', '0', '5000', 'ch-215-64.hd-190-10.hr-893-40.lg-275-1408', 'M', 'Â¡Nuev@ en Habbo!', '1528380419', '0', '0', '127.0.0.1', null, '', '0', '0', '0', '0', '0', '0', '100,100,100', '0', '', '0', '0', '0', '0', '0', '0', '', '', '', '0', '0', '1', '0', '1', '0', '1', '0', null, null, '1', null, null, null, null, '0', '0', null, '0', '0', '0', '0');
INSERT INTO `users` VALUES ('2', 'Stankman', null, 'defaultuser@meth0d.org', null, '11', '', '0', '88', '0', '', '0', '5000', 'ch-210-1211.sh-300-63.lg-285-64.ha-1002-63.hr-828-1061.hd-180-1', 'M', 'Â¡Nuev@ en Xukys!', '1529963402', '0', '0', '127.0.0.1', null, '', '0', '0', '0', '0', '0', '0', '100,100,100', '0', '', '0', '0', '0', '0', '0', '0', '', '', '', '0', '0', '1', '0', '1', '0', '1', '0', null, null, '1', null, null, null, null, '0', '0', null, '0', '0', '0', '0');
INSERT INTO `users` VALUES ('3', 'Norman', null, 'defaultuser@meth0d.org', null, '12', '', '0', '100', '0', '', '0', '5000', 'ch-210-1211.sh-300-63.lg-285-64.ha-1002-63.hr-828-1061.hd-180-1', 'M', 'Â¡Nuev@ en Xukys!', '1530214462', '0', '0', '127.0.0.1', null, '', '0', '0', '0', '0', '0', '0', '100,100,100', '0', '', '0', '0', '0', '0', '0', '0', '', '', '', '0', '0', '1', '0', '1', '0', '1', '0', null, null, '1', null, null, null, null, '0', '0', null, '0', '0', '0', '0');
INSERT INTO `users` VALUES ('4', 'Testing2', null, 'defaultuser@meth0d.org', null, '12', '', '0', '100', '0', '', '0', '5000', 'ch-210-1211.sh-300-63.lg-285-64.ha-1002-63.hr-828-1061.hd-180-1', 'M', 'Â¡Nuev@ en Xukys!', '1530214579', '0', '0', '127.0.0.1', null, '', '0', '0', '0', '0', '0', '0', '100,100,100', '0', '', '0', '0', '0', '0', '0', '0', '', '', '', '0', '0', '1', '0', '1', '0', '1', '0', null, null, '1', null, null, null, null, '0', '0', null, '0', '0', '0', '0');
INSERT INTO `users` VALUES ('5', 'Rory22', null, 'defaultuser@meth0d.org', null, '10', '', '0', '100', '0', '', '0', '5000', 'ch-210-1211.sh-300-63.lg-285-64.ha-1002-63.hr-828-1061.hd-180-1', 'M', 'Â¡Nuev@ en Xukys!', '1530214736', '0', '0', '127.0.0.1', null, '', '0', '0', '0', '0', '0', '0', '100,100,100', '0', '', '0', '0', '0', '0', '0', '0', '', '', '', '0', '0', '1', '0', '1', '0', '1', '0', null, null, '1', null, null, null, null, '0', '0', null, '0', '0', '0', '0');
INSERT INTO `users` VALUES ('6', 'Habbo2', null, 'defaultuser@meth0d.org', null, '13', '', '0', '100', '0', '', '0', '5000', 'ch-210-1211.sh-300-63.lg-285-64.ha-1002-63.hr-828-1061.hd-180-1', 'M', 'Â¡Nuev@ en Xukys!', '1530214772', '0', '0', '127.0.0.1', null, '', '0', '0', '0', '0', '0', '0', '100,100,100', '0', '', '0', '0', '0', '0', '0', '0', '', '', '', '0', '0', '1', '0', '1', '0', '1', '0', null, null, '1', null, null, null, null, '0', '0', null, '0', '0', '0', '0');
INSERT INTO `users` VALUES ('7', 'Usuaurio', null, 'defaultuser@meth0d.org', null, '1', '', '0', '100', '0', '', '0', '5000', 'ch-210-1211.sh-300-63.lg-285-64.ha-1002-63.hr-828-1061.hd-180-1', 'M', 'Â¡Nuev@ en Xukys!', '1530214903', '0', '0', '127.0.0.1', null, '', '0', '0', '0', '0', '0', '0', '100,100,100', '0', '', '0', '0', '0', '0', '0', '0', '', '', '', '0', '0', '1', '0', '1', '0', '1', '0', null, null, '1', null, null, null, null, '0', '0', null, '0', '0', '0', '0');
INSERT INTO `users` VALUES ('8', 'Gutierritos', null, 'defaultuser@meth0d.org', null, '1', '', '0', '100', '0', '', '0', '5000', 'ch-210-1211.sh-300-63.lg-285-64.ha-1002-63.hr-828-1061.hd-180-1', 'M', 'Â¡Nuev@ en Xukys!', '1530222193', '0', '0', '127.0.0.1', null, '', '0', '0', '0', '0', '0', '0', '100,100,100', '0', '', '0', '0', '0', '0', '0', '0', '', '', '', '0', '0', '1', '0', '1', '0', '1', '0', null, null, '1', null, null, null, null, '0', '0', null, '0', '0', '0', '0');
INSERT INTO `users` VALUES ('9', 'Xukys', null, 'defaultuser@meth0d.org', null, '1', '', '0', '100', '0', '', '0', '5000', 'ch-210-1211.sh-300-63.lg-285-64.ha-1002-63.hr-828-1061.hd-180-1', 'M', 'Â¡Nuev@ en Xukys!', '1530294287', '0', '0', '127.0.0.1', null, '', '0', '0', '0', '0', '0', '0', '100,100,100', '0', '', '0', '0', '0', '0', '0', '0', '', '', '', '0', '0', '1', '0', '1', '0', '1', '0', null, null, '1', null, null, null, null, '0', '0', null, '0', '0', '0', '0');
INSERT INTO `users` VALUES ('10', 'Xukys2', null, 'defaultuser@meth0d.org', null, '1', '', '0', '100', '0', '', '0', '5000', 'ch-210-1211.sh-300-63.lg-285-64.ha-1002-63.hr-828-1061.hd-180-1', 'M', 'Â¡Nuev@ en Xukys!', '1530294460', '0', '0', '127.0.0.1', null, '', '0', '0', '0', '0', '0', '0', '100,100,100', '0', '', '0', '0', '0', '0', '0', '0', '', '', '', '0', '0', '1', '0', '1', '0', '1', '0', null, null, '1', null, null, null, null, '0', '0', null, '0', '0', '0', '0');
INSERT INTO `users` VALUES ('11', 'ForbiMLP', null, 'defaultuser@meth0d.org', null, '1', '', '0', '100', '0', '', '0', '5000', 'ch-210-1211.sh-300-63.lg-285-64.ha-1002-63.hr-828-1061.hd-180-1', 'M', 'Â¡Nuev@ en Xukys!', '1530295662', '0', '0', '127.0.0.1', null, '', '0', '0', '0', '0', '0', '0', '100,100,100', '0', '', '0', '0', '0', '0', '0', '0', '', '', '', '0', '0', '1', '0', '1', '0', '1', '0', null, null, '1', null, null, null, null, '0', '0', null, '0', '0', '0', '0');
SET FOREIGN_KEY_CHECKS=1;
