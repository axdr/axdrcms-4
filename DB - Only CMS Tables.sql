/*
Navicat MySQL Data Transfer

Source Server         : phpmyadmin
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : plus

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2018-07-07 00:55:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for xdr_users
-- ----------------------------
DROP TABLE IF EXISTS `xdr_users`;
CREATE TABLE `xdr_users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '',
  `birth` varchar(10) NOT NULL DEFAULT '01-01-2013',
  `rpx_id` varchar(200) NOT NULL DEFAULT '',
  `rpx_type` enum('0','1','2') NOT NULL DEFAULT '0',
  `homeBg` varchar(120) NOT NULL DEFAULT 'b_bg_pattern_abstract2',
  `web_online` varchar(100) NOT NULL DEFAULT '',
  `visibility` enum('NOBODY','EVERYONE') NOT NULL DEFAULT 'EVERYONE',
  `predermited` enum('0','1') NOT NULL DEFAULT '1',
  `client_token` varchar(51) NOT NULL DEFAULT '',
  `token` varchar(21) NOT NULL DEFAULT '',
  `AddonData` text NOT NULL,
  `AccountID` bigint(255) NOT NULL DEFAULT '0',
  `AccountName` varchar(255) NOT NULL DEFAULT '',
  `receptionPased` enum('0','1') NOT NULL DEFAULT '0',
  `RememberMeToken` text NOT NULL,
  `vinculId` int(255) NOT NULL DEFAULT '0',
  `browser` varchar(255) NOT NULL DEFAULT '',
  `securityTokens` text NOT NULL,
  `country_cf` varchar(100) NOT NULL,
  `ip_current` varchar(45) NOT NULL,
  `task` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `mail` (`mail`) USING HASH,
  KEY `password` (`password`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdr_users
-- ----------------------------
INSERT INTO `xdr_users` VALUES ('1', 'el-timador96@hotmail.com', 'eUY71Z1FQ/09M9NhgEZs3RgYZLwh1tF2', '', '883956185315', '0', 'b_bg_pattern_bobbaskulls1', '1530817652', 'EVERYONE', '1', '', '717tsGJZi5', '', '0', '', '0', '', '0', '', '', 'ZZ', '127.0.0.1', '');
INSERT INTO `xdr_users` VALUES ('2', 'mustache_km@hotmail.com', 'eUY71Z1FQ/09M9NhgEZs3RgYZLwh1tF2', '', '848136942686', '0', 'b_bg_pattern_tinyroom', '1530064375', 'EVERYONE', '1', '', '8TPTazrQ6Y', '', '0', '', '0', '', '0', '', '', 'ZZ', '127.0.0.1', '');
INSERT INTO `xdr_users` VALUES ('3', 'nomran@hotmail.com', 'eUY71Z1FQ/09M9NhgEZs3RgYZLwh1tF2', '', '830362113660', '0', 'b_bg_pattern_abstract2', '1530214495', 'EVERYONE', '1', '', '3Ffo2pmCmR', '', '0', '', '0', '', '0', '', '', 'ZZ', '127.0.0.1', '');
INSERT INTO `xdr_users` VALUES ('4', 'textsing2@oasdas.com', 'eUY71Z1FQ/09M9NhgEZs3RgYZLwh1tF2', '', '486178155740', '0', 'b_bg_pattern_abstract2', '1530214706', 'EVERYONE', '1', '', 'KJnpWvYrhK', '', '0', '', '0', '', '0', '', '', 'ZZ', '127.0.0.1', '');
INSERT INTO `xdr_users` VALUES ('5', 'el-timador96as@hotmail.com', 'eUY71Z1FQ/09M9NhgEZs3RgYZLwh1tF2', '', '32931354118', '0', 'b_bg_pattern_abstract2', '1530214750', 'EVERYONE', '1', '', 'Ib9gSG1Otl', '', '0', '', '0', '', '0', '', '', 'ZZ', '127.0.0.1', '');
INSERT INTO `xdr_users` VALUES ('6', 'asdasd@asdasd.com', 'eUY71Z1FQ/09M9NhgEZs3RgYZLwh1tF2', '', '990469083188', '0', 'b_bg_pattern_abstract2', '1530214788', 'EVERYONE', '1', '', 'Nc1stlXvNX', '', '0', '', '0', '', '0', '', '', 'ZZ', '127.0.0.1', '');
INSERT INTO `xdr_users` VALUES ('7', 'usuaruoi@hotmail.com', 'eUY71Z1FQ/09M9NhgEZs3RgYZLwh1tF2', '', '733040948314', '0', 'b_bg_pattern_abstract2', '1530215259', 'EVERYONE', '1', '', '7JRpyyjfor', '', '0', '', '0', '', '0', '', '', 'ZZ', '127.0.0.1', '');
INSERT INTO `xdr_users` VALUES ('8', 'gutierrss@hotmail.com', 'eUY71Z1FQ/09M9NhgEZs3RgYZLwh1tF2', '', '379477039965', '0', 'b_bg_pattern_abstract2', '1530222198', 'EVERYONE', '1', '', 'Dg3ZzN4HUF', '', '0', '', '0', '', '0', '', '', 'ZZ', '127.0.0.1', '');
INSERT INTO `xdr_users` VALUES ('9', 'el-xukys@hotmail.com', 'eUY71Z1FQ/09M9NhgEZs3RgYZLwh1tF2', '', '423049981964', '0', 'b_bg_pattern_abstract2', '--', 'EVERYONE', '1', '', '', '', '0', '', '0', '', '0', '', '', '', '', '');
INSERT INTO `xdr_users` VALUES ('10', 'xukys@hotmail.com', 'eUY71Z1FQ/09M9NhgEZs3RgYZLwh1tF2', '', '483447349265', '0', 'b_bg_pattern_abstract2', '1530294504', 'EVERYONE', '1', '', '3CvLt4vlpG', '', '0', '', '0', '', '0', '', '', 'ZZ', '127.0.0.1', '');
INSERT INTO `xdr_users` VALUES ('11', 'gols@hotmail.com', 'eUY71Z1FQ/09M9NhgEZs3RgYZLwh1tF2', '', '610638852567', '0', 'b_bg_pattern_abstract2', '1530295695', 'EVERYONE', '1', '', 'v6jYYWgGvx', '', '0', '', '0', '', '0', '', '', 'ZZ', '127.0.0.1', '');

-- ----------------------------
-- Table structure for xdrcms_acp_comments
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_acp_comments`;
CREATE TABLE `xdrcms_acp_comments` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `userID` int(255) NOT NULL,
  `Created` int(255) NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Body` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_acp_comments
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_addons
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_addons`;
CREATE TABLE `xdrcms_addons` (
  `id` int(220) NOT NULL AUTO_INCREMENT,
  `title` varchar(61) NOT NULL,
  `category` int(5) NOT NULL DEFAULT '1',
  `color` int(1) NOT NULL DEFAULT '0',
  `rank` int(1) NOT NULL DEFAULT '0',
  `internal` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `template` enum('0','1') NOT NULL DEFAULT '1',
  `creatorId` int(255) NOT NULL DEFAULT '0',
  `created` int(255) NOT NULL DEFAULT '0',
  `canDisable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_addons
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_aio_config
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_aio_config`;
CREATE TABLE `xdrcms_aio_config` (
  `Name` varchar(255) NOT NULL,
  `Data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_aio_config
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_categories
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_categories`;
CREATE TABLE `xdrcms_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_categories
-- ----------------------------
INSERT INTO `xdrcms_categories` VALUES ('1', 'Ocultos');
INSERT INTO `xdrcms_categories` VALUES ('2', 'Staffs');
INSERT INTO `xdrcms_categories` VALUES ('3', 'Alfas');

-- ----------------------------
-- Table structure for xdrcms_guestbook
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_guestbook`;
CREATE TABLE `xdrcms_guestbook` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `message` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `time` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `widget_id` int(10) DEFAULT NULL,
  `userid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_guestbook
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_looks
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_looks`;
CREATE TABLE `xdrcms_looks` (
  `Look` text CHARACTER SET latin1 NOT NULL,
  `Gender` enum('M','F') CHARACTER SET latin1 NOT NULL,
  `Code` text CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_looks
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_minimail
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_minimail`;
CREATE TABLE `xdrcms_minimail` (
  `Id` int(255) NOT NULL AUTO_INCREMENT,
  `OwnerId` int(255) NOT NULL DEFAULT '0',
  `SenderId` int(255) NOT NULL DEFAULT '0',
  `ToIds` varchar(255) NOT NULL DEFAULT '0',
  `IsReaded` tinyint(1) NOT NULL DEFAULT '0',
  `InBin` tinyint(1) NOT NULL DEFAULT '0',
  `RelatedId` int(255) NOT NULL DEFAULT '0',
  `Title` varchar(255) NOT NULL DEFAULT 'Test Message',
  `Message` text,
  `Created` int(14) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_minimail
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_minimail_campaigns
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_minimail_campaigns`;
CREATE TABLE `xdrcms_minimail_campaigns` (
  `Id` int(255) NOT NULL AUTO_INCREMENT,
  `Creator` int(255) NOT NULL DEFAULT '0',
  `Title` varchar(255) NOT NULL DEFAULT 'Hello',
  `Message` text,
  `Image` varchar(255) NOT NULL DEFAULT 'http://images.habbo.com/c_images/album3236/em_wbk_2.gif',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_minimail_campaigns
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_news
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_news`;
CREATE TABLE `xdrcms_news` (
  `Id` int(255) NOT NULL AUTO_INCREMENT,
  `OwnerID` int(255) NOT NULL DEFAULT '0',
  `Title` varchar(255) NOT NULL,
  `Category` int(2) NOT NULL DEFAULT '1',
  `Summary` text NOT NULL,
  `Body` text NOT NULL,
  `BackGroundImage` varchar(255) NOT NULL DEFAULT '',
  `Images` text NOT NULL,
  `TimeCreated` int(255) NOT NULL DEFAULT '0',
  `Visible` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_news
-- ----------------------------
INSERT INTO `xdrcms_news` VALUES ('1', '1', '&iexcl;Muy Pronto!', '0', 'Xukys Hotel, volver&aacute; a abrir sus puertas en poco tiempo...', '&lt;p&gt;Esto solo es una prueba.&lt;/p&gt;', 'https://images.habbo.com/web_images/habbo-web-articles/HSE-TopStory-NEW.png', '', '1529905455', '1');

-- ----------------------------
-- Table structure for xdrcms_permissions
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_permissions`;
CREATE TABLE `xdrcms_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_permissions
-- ----------------------------
INSERT INTO `xdrcms_permissions` VALUES ('1', 'ase.access');
INSERT INTO `xdrcms_permissions` VALUES ('2', 'ase.site');
INSERT INTO `xdrcms_permissions` VALUES ('3', 'ase.server');
INSERT INTO `xdrcms_permissions` VALUES ('4', 'ase.swfs');
INSERT INTO `xdrcms_permissions` VALUES ('5', 'ase.users_page');
INSERT INTO `xdrcms_permissions` VALUES ('6', 'ase.catalog');
INSERT INTO `xdrcms_permissions` VALUES ('7', 'ase.shop_page');
INSERT INTO `xdrcms_permissions` VALUES ('8', 'ase.logs');
INSERT INTO `xdrcms_permissions` VALUES ('9', 'ase.edit_delete');
INSERT INTO `xdrcms_permissions` VALUES ('10', 'ase.delete_logs');
INSERT INTO `xdrcms_permissions` VALUES ('11', 'ase.edit_users');
INSERT INTO `xdrcms_permissions` VALUES ('12', 'ase.give_rank');
INSERT INTO `xdrcms_permissions` VALUES ('13', 'ase.ban_unban');
INSERT INTO `xdrcms_permissions` VALUES ('14', 'ase.alerts');
INSERT INTO `xdrcms_permissions` VALUES ('15', 'ase.uploads');
INSERT INTO `xdrcms_permissions` VALUES ('16', 'ase.articles');
INSERT INTO `xdrcms_permissions` VALUES ('17', 'web.daily_login');
INSERT INTO `xdrcms_permissions` VALUES ('18', 'web.advertisement');
INSERT INTO `xdrcms_permissions` VALUES ('19', 'web.monthly_present');

-- ----------------------------
-- Table structure for xdrcms_plugins
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_plugins`;
CREATE TABLE `xdrcms_plugins` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) DEFAULT NULL,
  `OwnerID` int(5) DEFAULT NULL,
  `Position` int(5) DEFAULT NULL,
  `minRank` int(5) DEFAULT NULL,
  `Template` enum('0','1') DEFAULT '1',
  `canDisable` enum('0','1') DEFAULT '1',
  `canDelete` enum('0','1') DEFAULT '1',
  `Color` int(11) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_plugins
-- ----------------------------
INSERT INTO `xdrcms_plugins` VALUES ('1', 'Estadisticas del Hotel', '1', '3', '1', '1', '0', '0', '2');
INSERT INTO `xdrcms_plugins` VALUES ('2', 'Nuestro Facebook', '1', '4', '1', '1', '0', '0', '3');
INSERT INTO `xdrcms_plugins` VALUES ('3', 'Nuestro Twitter', '1', '3', '0', '1', '0', '0', '1');
INSERT INTO `xdrcms_plugins` VALUES ('4', 'Mis Placas', '1', '3', '2', '1', '0', '0', '8');
INSERT INTO `xdrcms_plugins` VALUES ('9', 'hola', '1', '3', '1', '1', '0', '1', '7');
INSERT INTO `xdrcms_plugins` VALUES ('10', 'Prueba para Leo', '1', '4', '1', '1', '0', '1', '9');
INSERT INTO `xdrcms_plugins` VALUES ('11', 'Hola Gabo', '1', '3', '1', '1', '1', '1', '5');
INSERT INTO `xdrcms_plugins` VALUES ('13', 'Argentina vs Nigeria', '1', '3', '1', '1', '0', '1', '3');

-- ----------------------------
-- Table structure for xdrcms_promos
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_promos`;
CREATE TABLE `xdrcms_promos` (
  `Id` int(255) NOT NULL AUTO_INCREMENT,
  `OwnerID` int(255) NOT NULL DEFAULT '0',
  `Title` varchar(80) NOT NULL DEFAULT '',
  `Content` text NOT NULL,
  `BackGroundImage` text NOT NULL,
  `GoToHTML` text NOT NULL,
  `TimeCreated` varchar(255) NOT NULL DEFAULT '0-0-0',
  `Visible` enum('0','1') NOT NULL DEFAULT '1',
  `Button` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_promos
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_ranks
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_ranks`;
CREATE TABLE `xdrcms_ranks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT '1',
  `ase.1` enum('0','1') DEFAULT '0',
  `ase.2` enum('0','1') DEFAULT '0',
  `ase.3` enum('0','1') DEFAULT '0',
  `ase.4` enum('0','1') DEFAULT '0',
  `ase.5` enum('0','1') DEFAULT '0',
  `ase.6` enum('0','1') DEFAULT '0',
  `ase.7` enum('0','1') DEFAULT '0',
  `ase.8` enum('0','1') DEFAULT '0',
  `ase.9` enum('0','1') DEFAULT '0',
  `ase.10` enum('0','1') DEFAULT '0',
  `ase.11` enum('0','1') DEFAULT '0',
  `ase.12` enum('0','1') DEFAULT '0',
  `ase.13` enum('0','1') DEFAULT '0',
  `ase.14` enum('0','1') DEFAULT '0',
  `ase.15` enum('0','1') DEFAULT '0',
  `ase.16` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_ranks
-- ----------------------------
INSERT INTO `xdrcms_ranks` VALUES ('1', 'Membresia para apuestas', '2', '4', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('2', 'Apuestas', '3', '4', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('3', 'Construccion', '4', '4', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('4', 'Multimedia', '5', '4', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('5', 'Publicidad', '6', '3', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('6', 'Soporte al usuario', '7', '3', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('7', 'Candidatos', '8', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('8', 'Aspirantes', '9', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('9', 'Entretenimiento', '10', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('10', 'Seguridad', '11', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('11', 'Administracion General', '12', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('12', 'Coordinacion', '13', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('13', 'Desarrolladores', '14', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for xdrcms_shop
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_shop`;
CREATE TABLE `xdrcms_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('item','badge','voucher') NOT NULL DEFAULT 'item',
  `code` varchar(200) DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `limit` int(11) NOT NULL DEFAULT '1',
  `current` int(11) NOT NULL DEFAULT '0',
  `enabled` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of xdrcms_shop
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_site_inventory_items
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_site_inventory_items`;
CREATE TABLE `xdrcms_site_inventory_items` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `userId` int(100) NOT NULL,
  `var` varchar(100) CHARACTER SET latin1 NOT NULL,
  `skin` varchar(100) CHARACTER SET latin1 NOT NULL,
  `type` enum('Sticker','Background','WebCommodity') CHARACTER SET latin1 NOT NULL,
  `isWaiting` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COMMENT='Stickies del Sitio';

-- ----------------------------
-- Records of xdrcms_site_inventory_items
-- ----------------------------
INSERT INTO `xdrcms_site_inventory_items` VALUES ('1', '1', '', 'b_bg_lace', 'Background', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('2', '1', '', 'b_bg_pattern_bobbaskulls1', 'Background', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('13', '1', '', 's_stickers_mexico', 'Sticker', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('25', '1', '', '', 'Sticker', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('12', '1', '', 'b_bg_pattern_deepred', 'Background', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('10', '1', '', '', 'Sticker', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('26', '1', '0', '', 'Sticker', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('49', '1', '', 'commodity_stickienote_pre', 'WebCommodity', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('19', '1', '', 'b_bg_serpntine_darkred', 'Background', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('45', '1', '', 'commodity_stickienote_pre', 'WebCommodity', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('44', '1', '', 'commodity_stickienote_pre', 'WebCommodity', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('50', '1', '', 'commodity_stickienote_pre', 'WebCommodity', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('40', '2', '', 'b_bg_pattern_tinyroom', 'Background', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('48', '1', '', 'commodity_stickienote_pre', 'WebCommodity', '0');
INSERT INTO `xdrcms_site_inventory_items` VALUES ('47', '1', '', 'commodity_stickienote_pre', 'WebCommodity', '0');

-- ----------------------------
-- Table structure for xdrcms_site_items
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_site_items`;
CREATE TABLE `xdrcms_site_items` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `userId` int(100) NOT NULL DEFAULT '0',
  `groupId` int(10) NOT NULL DEFAULT '0',
  `position_left` int(4) NOT NULL,
  `position_top` int(4) NOT NULL,
  `position_z` int(4) NOT NULL,
  `var` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `skin` varchar(100) CHARACTER SET latin1 NOT NULL,
  `content` text CHARACTER SET latin1,
  `type` enum('stickie','sticker','widget') CHARACTER SET latin1 NOT NULL DEFAULT 'sticker',
  `Temporal` enum('True','False') CHARACTER SET latin1 NOT NULL DEFAULT 'False',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='Stickies del Sitio';

-- ----------------------------
-- Records of xdrcms_site_items
-- ----------------------------
INSERT INTO `xdrcms_site_items` VALUES ('1', '1', '0', '342', '116', '272', 'ProfileWidget', 'w_skin_goldenskin', '', 'widget', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('2', '2', '0', '262', '39', '128', 'ProfileWidget', 'w_skin_defaultskin', '', 'widget', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('49', '9', '0', '457', '26', '4', 'ProfileWidget', 'w_skin_defaultskin', '', 'widget', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('4', '1', '0', '612', '19', '70', '', 's_blue_diner_s', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('5', '1', '0', '198', '22', '239', '', 's_blue_diner_x', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('6', '1', '0', '301', '20', '237', '', 's_blue_diner_u', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('7', '1', '0', '404', '19', '235', '', 's_blue_diner_k', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('8', '1', '0', '510', '19', '68', '', 's_blue_diner_y', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('9', '1', '0', '718', '15', '72', '', 's_sticker_heartbeat', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('22', '1', '0', '159', '174', '94', '', 's_sh_sticker_redbolo_flipped', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('11', '0', '0', '10', '10', '8', '', '5', '[size=large][color=red]Probando estas notitas[/color][/size]', 'stickie', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('12', '0', '0', '10', '10', '8', '', '5', '[size=large][color=red]Probando las nuevas notitas de Xukys blah blah[/color][/size]', 'stickie', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('21', '1', '0', '164', '187', '90', '', 's_sticker_squelette', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('19', '1', '0', '742', '137', '175', '', 's_sticker_signfont_a', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('20', '1', '0', '108', '163', '233', '', 's_gothic_scienceplatform', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('23', '1', '0', '452', '197', '274', '', 's_sticker_gurubeard_gray', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('24', '1', '0', '66', '387', '160', 'RoomsWidget', 'w_skin_goldenskin', '', 'widget', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('25', '1', '0', '380', '388', '166', 'GuestbookWidget', 'w_skin_goldenskin', '', 'widget', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('26', '1', '0', '69', '625', '150', 'BadgesWidget', 'w_skin_goldenskin', '', 'widget', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('27', '1', '0', '420', '658', '168', 'FriendsWidget', 'w_skin_goldenskin', '', 'widget', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('28', '2', '0', '19', '41', '60', '', 's_wood_s', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('29', '2', '0', '105', '40', '64', '', 's_wood_a', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('30', '2', '0', '151', '103', '78', '', 's_wood_a', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('31', '2', '0', '195', '103', '80', '', 's_wood_n', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('32', '2', '0', '150', '40', '66', '', 's_wood_n', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('33', '2', '0', '62', '41', '62', '', 's_wood_t', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('34', '2', '0', '194', '40', '68', '', 's_wood_k', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('35', '2', '0', '99', '103', '76', '', 's_wood_m', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('36', '2', '0', '18', '101', '112', '', 's_sticker_cactus_anim', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('37', '2', '0', '8', '80', '110', '', 's_battle1', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('38', '2', '0', '335', '126', '116', '', 's_sticker_arrow_right', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('39', '3', '0', '457', '26', '4', 'ProfileWidget', 'w_skin_defaultskin', '', 'widget', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('40', '10', '0', '180', '366', '11', '', 's_paper_clip_1', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('41', '10', '0', '130', '22', '10', '', 's_needle_3', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('42', '10', '0', '280', '343', '3', '', 's_sticker_spaceduck', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('43', '10', '0', '593', '11', '9', '', 's_sticker_arrow_down', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('44', '10', '0', '107', '402', '8', '', 'n_skin_notepadskin', 'note.myfriends', 'stickie', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('45', '10', '0', '57', '229', '6', '', 'n_skin_speechbubbleskin', 'note.welcome', 'stickie', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('46', '10', '0', '148', '41', '7', '', 'n_skin_noteitskin', 'note.remember.security', 'stickie', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('47', '10', '0', '457', '26', '4', 'ProfileWidget', 'w_skin_goldenskin', '', 'widget', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('48', '10', '0', '479', '296', '13', 'RoomsWidget', 'w_skin_notepadskin', '', 'widget', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('54', '1', '0', '94', '959', '278', '', '5', 'Al fin estas cosas estan funcionando carajo.', 'stickie', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('55', '11', '0', '180', '366', '11', '', 's_paper_clip_1', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('56', '11', '0', '130', '22', '10', '', 's_needle_3', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('57', '11', '0', '280', '343', '3', '', 's_sticker_spaceduck', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('58', '11', '0', '593', '11', '9', '', 's_sticker_arrow_down', '', 'sticker', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('59', '11', '0', '107', '402', '8', '', 'n_skin_notepadskin', 'note.myfriends', 'stickie', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('60', '11', '0', '57', '229', '6', '', 'n_skin_speechbubbleskin', 'note.welcome', 'stickie', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('61', '11', '0', '148', '41', '7', '', 'n_skin_noteitskin', 'note.remember.security', 'stickie', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('62', '11', '0', '457', '26', '4', 'ProfileWidget', 'w_skin_metalskin', '', 'widget', 'False');
INSERT INTO `xdrcms_site_items` VALUES ('63', '11', '0', '450', '319', '1', 'RoomsWidget', 'w_skin_notepadskin', '', 'widget', 'False');

-- ----------------------------
-- Table structure for xdrcms_staff_log
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_staff_log`;
CREATE TABLE `xdrcms_staff_log` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `action` varchar(12) NOT NULL,
  `message` text,
  `note` text,
  `userid` int(23) NOT NULL DEFAULT '0',
  `targetid` int(11) DEFAULT '0',
  `timestamp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_staff_log
-- ----------------------------
INSERT INTO `xdrcms_staff_log` VALUES ('1', 'Create', 'CreaciÃ³n del artÃ­culo \"&iexcl;Muy Pronto!\". Id asignada: 1', 'manage.php[articles]', '1', '0', '1529905455');
INSERT INTO `xdrcms_staff_log` VALUES ('2', 'Create', 'CreaciÃ³n del plugin \"Hola Gabo\". Id asignada: 11', 'manage.php[plugins]', '1', '0', '1529958378');
INSERT INTO `xdrcms_staff_log` VALUES ('3', 'Create', 'CreaciÃ³n del plugin \"Francia vs Dinamarca\". Id asignada: 12', 'manage.php[plugins]', '1', '0', '1530020135');
INSERT INTO `xdrcms_staff_log` VALUES ('4', 'Remove', 'Borrado de un plugin. Id del plugin: 12', 'manage.php[plugins]', '1', '0', '1530020595');
INSERT INTO `xdrcms_staff_log` VALUES ('5', 'Create', 'CreaciÃ³n del plugin \"Francia vs Dinamarca\". Id asignada: 13', 'manage.php[plugins]', '1', '0', '1530020615');
INSERT INTO `xdrcms_staff_log` VALUES ('6', 'Change', 'Editado el plugin \"Francia vs Dinamarca\". Id del plugin: 13', 'manage.php[plugins]', '1', '0', '1530028369');
INSERT INTO `xdrcms_staff_log` VALUES ('7', 'Change', 'Editado el plugin \"Francia vs Dinamarca\". Id del plugin: 13', 'manage.php[plugins]', '1', '0', '1530036980');
INSERT INTO `xdrcms_staff_log` VALUES ('8', 'Change', 'Editado el plugin \"Argentina vs Nigeria\". Id del plugin: 13', 'manage.php[plugins]', '1', '0', '1530037010');
INSERT INTO `xdrcms_staff_log` VALUES ('9', 'Change', 'Editado la configuraciÃ³n del sitio', 'manage.php[site]', '1', '0', '1530063694');
INSERT INTO `xdrcms_staff_log` VALUES ('10', 'Change', 'Editado la configuraciÃ³n del sitio', 'manage.php[site]', '1', '0', '1530222446');
INSERT INTO `xdrcms_staff_log` VALUES ('11', 'Change', 'Editado la configuraciÃ³n del sitio', 'manage.php[site]', '1', '0', '1530235222');

-- ----------------------------
-- Table structure for xdrcms_store_items
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_store_items`;
CREATE TABLE `xdrcms_store_items` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `price` int(10) NOT NULL,
  `skin` varchar(100) CHARACTER SET latin1 NOT NULL,
  `type` enum('Sticker','WebCommodity','Background','') CHARACTER SET latin1 NOT NULL DEFAULT 'Sticker',
  `categoryId` int(10) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `ItemName` varchar(120) CHARACTER SET latin1 NOT NULL,
  `ItemPack` int(10) NOT NULL DEFAULT '0',
  `ItemsContent` text CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2079 DEFAULT CHARSET=utf8 COMMENT='Stickies del Sitio';

-- ----------------------------
-- Records of xdrcms_store_items
-- ----------------------------
INSERT INTO `xdrcms_store_items` VALUES ('1460', '1', 's_stickers_uruguay', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1462', '1', 's_stickers_peru', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1456', '1', 's_stickers_paraguay', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1459', '1', 's_stickers_panama', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1454', '1', 's_stickers_nicaragua', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1465', '1', 's_stickers_mexico', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1455', '1', 's_stickers_honduras', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1452', '1', 's_stickers_espain', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1453', '1', 's_stickers_elsalvador', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1461', '1', 's_stickers_ecuador', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1458', '1', 's_stickers_costarica', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1464', '1', 's_stickers_colombia', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1467', '1', 's_stickers_chile', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1457', '1', 's_stickers_bolivia', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1466', '1', 's_stickers_argentina', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1463', '1', 's_stickers_venezuela', 'Sticker', '66', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('303', '1', 's_trax_sfx', 'Sticker', '27', '0', 'Trax Sonidos fx', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('307', '1', 's_trax_disco', 'Sticker', '27', '0', 'Trax Disco', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('300', '1', 's_trax_8_bit', 'Sticker', '27', '0', 'Trax Habbo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('305', '1', 's_trax_electro', 'Sticker', '27', '0', 'Trax Electrónica', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('304', '1', 's_trax_reggae', 'Sticker', '27', '0', 'Trax Reggae', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('301', '1', 's_trax_ambient', 'Sticker', '27', '0', 'Trax Ambiente', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('302', '1', 's_trax_bling', 'Sticker', '27', '0', 'Trax Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('306', '1', 's_trax_heavy', 'Sticker', '27', '0', 'Trax Heavy', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('308', '1', 's_trax_latin', 'Sticker', '27', '0', 'Trax Latina', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('651', '1', 's_trax_rock', 'Sticker', '27', '0', 'Trax Rock', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('530', '1', 's_a', 'Sticker', '29', '0', 'A', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('539', '1', 's_b_2', 'Sticker', '29', '0', 'B', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('540', '1', 's_c', 'Sticker', '29', '0', 'C', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('552', '1', 's_d', 'Sticker', '29', '0', 'D', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('556', '1', 's_e', 'Sticker', '29', '0', 'E', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('568', '1', 's_f', 'Sticker', '29', '0', 'F', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('569', '1', 's_g', 'Sticker', '29', '0', 'G', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('573', '1', 's_h', 'Sticker', '29', '0', 'H', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('574', '1', 's_i', 'Sticker', '29', '0', 'I', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('576', '1', 's_j', 'Sticker', '29', '0', 'J', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('577', '1', 's_k', 'Sticker', '29', '0', 'K', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('578', '1', 's_l', 'Sticker', '29', '0', 'L', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('584', '1', 's_m', 'Sticker', '29', '0', 'M', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('585', '1', 's_n', 'Sticker', '29', '0', 'N', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('595', '1', 's_o', 'Sticker', '29', '0', 'O', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('597', '1', 's_p', 'Sticker', '29', '0', 'P', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('603', '1', 's_q', 'Sticker', '29', '0', 'Q', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('604', '1', 's_r', 'Sticker', '29', '0', 'R', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('606', '1', 's_s', 'Sticker', '29', '0', 'S', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('650', '1', 's_t', 'Sticker', '29', '0', 'T', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('652', '1', 's_u', 'Sticker', '29', '0', 'U', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('655', '1', 's_v', 'Sticker', '29', '0', 'V', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('677', '1', 's_w', 'Sticker', '29', '0', 'W', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('679', '1', 's_x', 'Sticker', '29', '0', 'X', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('690', '1', 's_y', 'Sticker', '29', '0', 'Y', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('691', '1', 's_z', 'Sticker', '29', '0', 'Z', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('533', '1', 's_a_with_circle', 'Sticker', '29', '0', 'A', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('534', '1', 's_a_with_dots', 'Sticker', '29', '0', 'A', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('596', '1', 's_o_with_dots', 'Sticker', '29', '0', 'O', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('554', '1', 's_dot', 'Sticker', '29', '0', 'Punto', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('531', '1', 's_acsent1', 'Sticker', '29', '0', 'Apóstrofe', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('532', '1', 's_acsent2', 'Sticker', '29', '0', 'Tilde', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('654', '1', 's_underscore', 'Sticker', '29', '0', 'Guión', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('137', '1', 's_bling_star', 'Sticker', '30', '0', 'Estrella Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('107', '1', 's_bling_a', 'Sticker', '30', '0', 'A Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('108', '1', 's_bling_b', 'Sticker', '30', '0', 'B Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('109', '1', 's_bling_c', 'Sticker', '30', '0', 'C Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('110', '1', 's_bling_d', 'Sticker', '30', '0', 'D Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('111', '1', 's_bling_e', 'Sticker', '30', '0', 'E Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('112', '1', 's_bling_f', 'Sticker', '30', '0', 'F Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('113', '1', 's_bling_g', 'Sticker', '30', '0', 'G Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('114', '1', 's_bling_h', 'Sticker', '30', '0', 'H Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('115', '1', 's_bling_i', 'Sticker', '30', '0', 'I Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('116', '1', 's_bling_j', 'Sticker', '30', '0', 'J Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('117', '1', 's_bling_k', 'Sticker', '30', '0', 'K Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('118', '1', 's_bling_l', 'Sticker', '30', '0', 'L Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('119', '1', 's_bling_m', 'Sticker', '30', '0', 'M Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('120', '1', 's_bling_n', 'Sticker', '30', '0', 'N Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('121', '1', 's_bling_o', 'Sticker', '30', '0', 'O Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('122', '1', 's_bling_p', 'Sticker', '30', '0', 'P Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('123', '1', 's_bling_q', 'Sticker', '30', '0', 'Q Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('124', '1', 's_bling_r', 'Sticker', '30', '0', 'R Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('125', '1', 's_bling_s', 'Sticker', '30', '0', 'S Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('126', '1', 's_bling_t', 'Sticker', '30', '0', 'T Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('127', '1', 's_bling_u', 'Sticker', '30', '0', 'U Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('128', '1', 's_bling_v', 'Sticker', '30', '0', 'V Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('129', '1', 's_bling_w', 'Sticker', '30', '0', 'W Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('130', '1', 's_bling_x', 'Sticker', '30', '0', 'X Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('131', '1', 's_bling_y', 'Sticker', '30', '0', 'Y Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('132', '1', 's_bling_z', 'Sticker', '30', '0', 'Z Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('133', '1', 's_bling_underscore', 'Sticker', '30', '0', 'Guión Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('139', '1', 's_bling_comma', 'Sticker', '30', '0', 'Coma Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('138', '1', 's_bling_dot', 'Sticker', '30', '0', 'mypage.sticker.bling_dot.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('135', '1', 's_bling_exclamation', 'Sticker', '30', '0', 'Exclamación Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('136', '1', 's_bling_question', 'Sticker', '30', '0', 'Pregunta Bling', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('927', '1', 's_wood_a', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('932', '1', 's_wood_b', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('933', '1', 's_wood_c', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('935', '1', 's_wood_d', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('937', '1', 's_wood_e', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('939', '1', 's_wood_f', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('940', '1', 's_wood_g', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('941', '1', 's_wood_h', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('942', '1', 's_wood_i', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('943', '1', 's_wood_j', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('944', '1', 's_wood_k', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('945', '1', 's_wood_l', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('946', '1', 's_wood_m', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('947', '1', 's_wood_n', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('948', '1', 's_wood_o', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('950', '1', 's_wood_p', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('951', '1', 's_wood_q', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('953', '1', 's_wood_r', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('954', '1', 's_wood_s', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('955', '1', 's_wood_t', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('956', '1', 's_wood_u', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('958', '1', 's_wood_v', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('959', '1', 's_wood_w', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('960', '1', 's_wood_x', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('961', '1', 's_wood_y', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('962', '1', 's_wood_z', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('928', '1', 's_wood_acircle', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('931', '1', 's_wood_adots', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('949', '1', 's_wood_odots', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('936', '1', 's_wood_dot', 'Sticker', '55', '0', 'mypage.sticker.wood_dot.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('934', '1', 's_wood_comma', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('938', '1', 's_wood_exclamation', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('952', '1', 's_wood_question', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('929', '1', 's_wood_acsent', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('930', '1', 's_wood_acsent2', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('957', '1', 's_wood_undermark', 'Sticker', '55', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1025', '1', 's_blue_diner_zero', 'Sticker', '56', '0', '0', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1007', '1', 's_blue_diner_one', 'Sticker', '56', '0', '1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1017', '1', 's_blue_diner_two', 'Sticker', '56', '0', '2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1016', '1', 's_blue_diner_three', 'Sticker', '56', '0', '3', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('996', '1', 's_blue_diner_four', 'Sticker', '56', '0', '4', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('995', '1', 's_blue_diner_five', 'Sticker', '56', '0', '5', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1014', '1', 's_blue_diner_six', 'Sticker', '56', '0', '6', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1013', '1', 's_blue_diner_seven', 'Sticker', '56', '0', '7', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('992', '1', 's_blue_diner_eight', 'Sticker', '56', '0', '8', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1005', '1', 's_blue_diner_nine', 'Sticker', '56', '0', '9', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('987', '1', 's_blue_diner_a', 'Sticker', '56', '0', 'A', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1026', '1', 's_blue_diner_a_umlaut', 'Sticker', '56', '0', 'Ä', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1027', '1', 's_blue_diner_ae', 'Sticker', '56', '0', 'AE', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('988', '1', 's_blue_diner_b', 'Sticker', '56', '0', 'B', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('989', '1', 's_blue_diner_c', 'Sticker', '56', '0', 'C', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1028', '1', 's_blue_diner_c_cedilla', 'Sticker', '56', '0', 'Ç', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('990', '1', 's_blue_diner_d', 'Sticker', '56', '0', 'D', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('991', '1', 's_blue_diner_e', 'Sticker', '56', '0', 'E', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1029', '1', 's_blue_diner_e_acc', 'Sticker', '56', '0', 'É', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1030', '1', 's_blue_diner_e_acc_grave', 'Sticker', '56', '0', 'È', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('994', '1', 's_blue_diner_f', 'Sticker', '56', '0', 'F', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('997', '1', 's_blue_diner_g', 'Sticker', '56', '0', 'G', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('998', '1', 's_blue_diner_h', 'Sticker', '56', '0', 'H', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('999', '1', 's_blue_diner_i', 'Sticker', '56', '0', 'I', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1000', '1', 's_blue_diner_j', 'Sticker', '56', '0', 'J', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1001', '1', 's_blue_diner_k', 'Sticker', '56', '0', 'K', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1002', '1', 's_blue_diner_l', 'Sticker', '56', '0', 'L', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1003', '1', 's_blue_diner_m', 'Sticker', '56', '0', 'M', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1004', '1', 's_blue_diner_n', 'Sticker', '56', '0', 'N', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1006', '1', 's_blue_diner_o', 'Sticker', '56', '0', 'O', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1031', '1', 's_blue_diner_o_accute', 'Sticker', '56', '0', 'Ó', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1032', '1', 's_blue_diner_o_cc_grave', 'Sticker', '56', '0', 'Ò', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1033', '1', 's_blue_diner_o_umlaut', 'Sticker', '56', '0', 'Ö', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1008', '1', 's_blue_diner_p', 'Sticker', '56', '0', 'P', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1009', '1', 's_blue_diner_q', 'Sticker', '56', '0', 'Q', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1011', '1', 's_blue_diner_r', 'Sticker', '56', '0', 'R', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1012', '1', 's_blue_diner_s', 'Sticker', '56', '0', 'S', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1015', '1', 's_blue_diner_t', 'Sticker', '56', '0', 'T', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1018', '1', 's_blue_diner_u', 'Sticker', '56', '0', 'U', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1034', '1', 's_blue_diner_u_acc', 'Sticker', '56', '0', 'Ú', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1035', '1', 's_blue_diner_u_acc_grave', 'Sticker', '56', '0', 'Ù', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1036', '1', 's_blue_diner_u_umlaut', 'Sticker', '56', '0', 'Ü', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1020', '1', 's_blue_diner_v', 'Sticker', '56', '0', 'V', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1021', '1', 's_blue_diner_w', 'Sticker', '56', '0', 'W', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1022', '1', 's_blue_diner_x', 'Sticker', '56', '0', 'X', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1023', '1', 's_blue_diner_y', 'Sticker', '56', '0', 'Y', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1024', '1', 's_blue_diner_z', 'Sticker', '56', '0', 'Z', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('993', '1', 's_blue_diner_exclamation', 'Sticker', '56', '0', '!', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1019', '1', 's_blue_diner_upsidedown_pre', 'Sticker', '56', '0', '¿', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1010', '1', 's_blue_diner_question', 'Sticker', '56', '0', '?', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1075', '1', 's_green_diner_zero', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1057', '1', 's_green_diner_one', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1067', '1', 's_green_diner_two', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1066', '1', 's_green_diner_three', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1046', '1', 's_green_diner_four', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1045', '1', 's_green_diner_five', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1064', '1', 's_green_diner_six', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1063', '1', 's_green_diner_seven', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1042', '1', 's_green_diner_eight', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1055', '1', 's_green_diner_nine', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1037', '1', 's_green_diner_a', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1076', '1', 's_green_diner_a_umlaut', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1077', '1', 's_green_diner_ae', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1038', '1', 's_green_diner_b', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1039', '1', 's_green_diner_c', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1078', '1', 's_green_diner_c_cedilla', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1040', '1', 's_green_diner_d', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1041', '1', 's_green_diner_e', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1079', '1', 's_green_diner_e_acc', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1080', '1', 's_green_diner_e_cc_grave', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1044', '1', 's_green_diner_f', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1047', '1', 's_green_diner_g', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1048', '1', 's_green_diner_h', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1049', '1', 's_green_diner_i', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1050', '1', 's_green_diner_j', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1051', '1', 's_green_diner_k', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1052', '1', 's_green_diner_l', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1053', '1', 's_green_diner_m', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1054', '1', 's_green_diner_n', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1056', '1', 's_green_diner_o', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1081', '1', 's_green_diner_o_accute', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1082', '1', 's_green_diner_o_cc_grave', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1083', '1', 's_green_diner_o_umlaut', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1058', '1', 's_green_diner_p', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1059', '1', 's_green_diner_q', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1061', '1', 's_green_diner_r', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1062', '1', 's_green_diner_s', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1065', '1', 's_green_diner_t', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1068', '1', 's_green_diner_u', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1084', '1', 's_green_diner_u_acc', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1085', '1', 's_green_diner_u_acc_grave', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1086', '1', 's_green_diner_u_umlaut', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1070', '1', 's_green_diner_v', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1071', '1', 's_green_diner_w', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1072', '1', 's_green_diner_x', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1073', '1', 's_green_diner_y', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1074', '1', 's_green_diner_z', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1043', '1', 's_green_diner_exclamation', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1069', '1', 's_green_diner_upsidedown_pre', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1060', '1', 's_green_diner_question', 'Sticker', '60', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1125', '1', 's_red_diner_zero', 'Sticker', '61', '0', '0', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1107', '1', 's_red_diner_one', 'Sticker', '61', '0', '1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1117', '1', 's_red_diner_two', 'Sticker', '61', '0', '2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1116', '1', 's_red_diner_three', 'Sticker', '61', '0', '3', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1096', '1', 's_red_diner_four', 'Sticker', '61', '0', '4', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1095', '1', 's_red_diner_five', 'Sticker', '61', '0', '5', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1114', '1', 's_red_diner_six', 'Sticker', '61', '0', '6', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1113', '1', 's_red_diner_seven', 'Sticker', '61', '0', '7', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1092', '1', 's_red_diner_eight', 'Sticker', '61', '0', '8', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1105', '1', 's_red_diner_nine', 'Sticker', '61', '0', '9', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1087', '1', 's_red_diner_a', 'Sticker', '61', '0', 'A', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1126', '1', 's_red_diner_a_umlaut', 'Sticker', '61', '0', 'Ä', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1127', '1', 's_red_diner_ae', 'Sticker', '61', '0', 'AE', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1088', '1', 's_red_diner_b', 'Sticker', '61', '0', 'B', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1089', '1', 's_red_diner_c', 'Sticker', '61', '0', 'C', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1128', '1', 's_red_diner_c_cedilla', 'Sticker', '61', '0', 'Ç', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1090', '1', 's_red_diner_d', 'Sticker', '61', '0', 'D', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1091', '1', 's_red_diner_e', 'Sticker', '61', '0', 'E', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1129', '1', 's_red_diner_e_acc', 'Sticker', '61', '0', 'É', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1130', '1', 's_red_diner_e_cc_grave', 'Sticker', '61', '0', 'È', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1094', '1', 's_red_diner_f', 'Sticker', '61', '0', 'F', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1097', '1', 's_red_diner_g', 'Sticker', '61', '0', 'G', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1098', '1', 's_red_diner_h', 'Sticker', '61', '0', 'H', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1099', '1', 's_red_diner_i', 'Sticker', '61', '0', 'I', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1100', '1', 's_red_diner_j', 'Sticker', '61', '0', 'J', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1101', '1', 's_red_diner_k', 'Sticker', '61', '0', 'K', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1102', '1', 's_red_diner_l', 'Sticker', '61', '0', 'L', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1103', '1', 's_red_diner_m', 'Sticker', '61', '0', 'M', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1104', '1', 's_red_diner_n', 'Sticker', '61', '0', 'N', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1106', '1', 's_red_diner_o', 'Sticker', '61', '0', 'O', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1131', '1', 's_red_diner_o_accute', 'Sticker', '61', '0', 'Ó', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1132', '1', 's_red_diner_o_cc_grave', 'Sticker', '61', '0', 'Ò', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1133', '1', 's_red_diner_o_umlaut', 'Sticker', '61', '0', 'Ö', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1108', '1', 's_red_diner_p', 'Sticker', '61', '0', 'P', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1109', '1', 's_red_diner_q', 'Sticker', '61', '0', 'Q', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1111', '1', 's_red_diner_r', 'Sticker', '61', '0', 'R', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1112', '1', 's_red_diner_s', 'Sticker', '61', '0', 'S', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1115', '1', 's_red_diner_t', 'Sticker', '61', '0', 'T', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1118', '1', 's_red_diner_u', 'Sticker', '61', '0', 'U', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1134', '1', 's_red_diner_u_acc', 'Sticker', '61', '0', 'Ú', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1135', '1', 's_red_diner_u_acc_grave', 'Sticker', '61', '0', 'Ù', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1136', '1', 's_red_diner_u_umlaut', 'Sticker', '61', '0', 'Ü', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1120', '1', 's_red_diner_v', 'Sticker', '61', '0', 'V', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1121', '1', 's_red_diner_w', 'Sticker', '61', '0', 'W', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1122', '1', 's_red_diner_x', 'Sticker', '61', '0', 'X', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1123', '1', 's_red_diner_y', 'Sticker', '61', '0', 'Y', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1124', '1', 's_red_diner_z', 'Sticker', '61', '0', 'Z', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1093', '1', 's_red_diner_exclamation', 'Sticker', '61', '0', '!', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1119', '1', 's_red_diner_upsidedown_pre', 'Sticker', '61', '0', '¿', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1110', '1', 's_red_diner_question', 'Sticker', '61', '0', '?', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('102', '1', 's_sticker_zipper_h_tile', 'Sticker', '31', '0', 'Cremallera', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('101', '1', 's_sticker_zipper_h_normal_lock', 'Sticker', '31', '0', 'Cremallera', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('100', '1', 's_sticker_zipper_h_end', 'Sticker', '31', '0', 'Cremallera', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('99', '1', 's_sticker_zipper_h_bobba_lock', 'Sticker', '31', '0', 'Cremallera', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('95', '1', 's_leafs2', 'Sticker', '31', '0', 'Hojas 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('94', '1', 's_leafs1', 'Sticker', '31', '0', 'Hojas', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('93', '1', 's_vine2', 'Sticker', '31', '0', 'Parra 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('92', '1', 's_vine', 'Sticker', '31', '0', 'Parra', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('50', '1', 's_ruler_vertical', 'Sticker', '31', '0', 'Regla', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('49', '1', 's_ruler_horizontal', 'Sticker', '31', '0', 'Regla', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('21', '1', 's_chain_horizontal', 'Sticker', '31', '0', 'Cadena 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('20', '1', 's_chain_vertical', 'Sticker', '31', '0', 'Cadena 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('103', '1', 's_sticker_zipper_v_tile', 'Sticker', '31', '0', 'Cremallera', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('104', '1', 's_sticker_zipper_v_bobba_lock', 'Sticker', '31', '0', 'Cremallera', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('105', '1', 's_sticker_zipper_v_end', 'Sticker', '31', '0', 'Cremallera', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('106', '1', 's_sticker_zipper_v_normal_lock', 'Sticker', '31', '0', 'Cremallera', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('678', '1', 's_wormhand', 'Sticker', '32', '0', 'Mano de gorila', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('714', '1', 's_sticker_gentleman', 'Sticker', '32', '0', 'Caballero', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('25', '1', 's_chewed_bubblegum', 'Sticker', '32', '0', 'Chicle', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('53', '1', 's_sticker_cactus_anim', 'Sticker', '32', '0', 'Cáctus', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('54', '1', 's_sticker_spaceduck', 'Sticker', '32', '0', 'Pato espacial', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('55', '1', 's_sticker_moonpig', 'Sticker', '32', '0', 'Cerdo volador', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('59', '1', 's_swimming_fish', 'Sticker', '32', '0', '¡A nadar!', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('66', '1', 's_sticker_boxer', 'Sticker', '32', '0', 'Boxeador', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('96', '1', 's_wunderfrank', 'Sticker', '32', '0', 'Frank', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('97', '1', 's_sticker_submarine', 'Sticker', '32', '0', 'submarino', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('65', '1', 's_sticker_clown_anim', 'Sticker', '32', '0', 'Payaso', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('16', '1', 's_blingblingstars', 'Sticker', '33', '0', 'Estrellas', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('17', '1', 's_blinghearts', 'Sticker', '33', '0', 'Corazones', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('18', '1', 's_sticker_heartbeat', 'Sticker', '33', '0', 'Latidos del corazón', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('56', '1', 's_sticker_catinabox', 'Sticker', '33', '0', 'Gato en una caja', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('58', '1', 's_bear', 'Sticker', '33', '0', 'Oso', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('19', '1', 's_sticker_bobbaskull', 'Sticker', '34', '0', 'calavera bobba', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('67', '1', 's_scubacapsule_anim', 'Sticker', '34', '0', 'Cápsula', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('633', '3', 'package_product_pre', '', '35', '3', 'mypage.product.hw_sticker_paper_clips.title', '0', 's_paper_clip_1,s_paper_clip_2,s_paper_clip_3');
INSERT INTO `xdrcms_store_items` VALUES ('710', '1', 's_sticker_flower1', 'Sticker', '35', '5', 'Flor', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('614', '3', 'package_product_pre', '', '35', '3', 'mypage.product.hw_spills.title', '0', 's_spill1,s_spill2,s_spill3');
INSERT INTO `xdrcms_store_items` VALUES ('707', '1', 's_icecube_big', 'Sticker', '35', '10', 'mypage.product.hw_package4_icecube_big.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('601', '1', 's_leafs2', 'Sticker', '35', '7', 'mypage.product.hw_package4_leafs1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('600', '1', 's_vine2', 'Sticker', '35', '5', 'mypage.product.hw_package3_vine2.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('599', '1', 's_chain_horizontal', 'Sticker', '35', '5', 'mypage.product.hw_package2_chain_horizontal.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('704', '3', 'package_product_pre', '', '35', '4', 'mypage.product.hw_package15_sticker_costumes2.title', '0', 's_scubacapsule_anim,s_sticker_eyes_blue,s_sticker_glasses_elton,s_sticker_glasses_supernerd');
INSERT INTO `xdrcms_store_items` VALUES ('598', '1', 's_sticker_zipper_v_tile', 'Sticker', '35', '5', 'mypage.product.hw_package1_sticker_zipper_v.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('583', '2', 'package_product_pre', '', '35', '2', 'mypage.product.hw_leafs.title', '0', 's_leafs1,s_leafs2');
INSERT INTO `xdrcms_store_items` VALUES ('602', '1', 's_icecube_small', 'Sticker', '35', '10', 'mypage.product.hw_package6_icecube_small.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('3', '1', 's_sticker_arrow_down', 'Sticker', '36', '0', 'Flecha abajo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('4', '1', 's_sticker_arrow_left', 'Sticker', '36', '0', 'Flecha izquierda', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('5', '1', 's_sticker_arrow_up', 'Sticker', '36', '0', 'Flecha arriba', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('7', '1', 's_finger_push', 'Sticker', '36', '0', 'Dedo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('6', '1', 's_sticker_arrow_right', 'Sticker', '36', '0', 'Flecha derecha', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('8', '1', 's_sticker_pointing_hand_1', 'Sticker', '36', '0', 'Señalando 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('9', '1', 's_sticker_pointing_hand_2', 'Sticker', '36', '0', 'Señalando 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('10', '1', 's_sticker_pointing_hand_3', 'Sticker', '36', '0', 'Señalando 3', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('11', '1', 's_sticker_pointing_hand_4', 'Sticker', '36', '0', 'Señalando 4', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('22', '1', 's_nail2', 'Sticker', '36', '0', 'Clavo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('23', '1', 's_nail3', 'Sticker', '36', '0', 'mypage.sticker.nail3.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('36', '1', 's_needle_1', 'Sticker', '36', '0', 'Aguja 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('37', '1', 's_needle_2', 'Sticker', '36', '0', 'Aguja 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('38', '1', 's_needle_3', 'Sticker', '36', '0', 'Aguja 3', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('39', '1', 's_needle_4', 'Sticker', '36', '0', 'Aguja 4', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('40', '1', 's_needle_5', 'Sticker', '36', '0', 'Aguja 5', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('89', '1', 's_sticker_flower1', 'Sticker', '37', '0', 'Flor', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('90', '1', 's_sticker_flower_big_yellow', 'Sticker', '37', '0', 'Flores amarillas', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('91', '1', 's_sticker_flower_pink', 'Sticker', '37', '0', 'Flores rosas', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('24', '1', 's_i_love_bobba', 'Sticker', '38', '0', 'Placa Bobba', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('77', '1', 's_i_love_coffee', 'Sticker', '38', '0', 'Placa Café', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('84', '1', 's_sticker_effect_bam', 'Sticker', '38', '0', '¡Pum!', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('85', '1', 's_sticker_effect_burp', 'Sticker', '38', '0', '¡Puff!', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('86', '1', 's_sticker_effect_woosh', 'Sticker', '38', '0', '¡Whau!', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('87', '1', 's_sticker_effect_zap', 'Sticker', '38', '0', '¡Flip!', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('88', '1', 's_sticker_effect_whoosh2', 'Sticker', '38', '0', '¡Whau 2!', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('697', '1', 's_icecube_small', 'Sticker', '39', '0', 'Cubito', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('621', '1', 's_ss_snowballmachine', 'Sticker', '39', '0', 'Máquina de bolas', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('575', '1', 's_icecube_big', 'Sticker', '39', '0', 'Cubito', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('617', '1', 's_bootsitjalapaset_red', 'Sticker', '39', '0', 'Botas y guantes', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('620', '1', 's_ss_hits_by_snowball', 'Sticker', '39', '0', 'Nieve', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('564', '1', 's_extra_ss_duck_left', 'Sticker', '39', '0', 'Pato nevado', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('626', '1', 's_ss_snowtree', 'Sticker', '39', '0', 'Árbol nevado', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('618', '1', 's_ss_costume_blue', 'Sticker', '39', '0', 'Traje, botas y guantes', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('565', '1', 's_extra_ss_duck_right', 'Sticker', '39', '0', 'Pato nevado', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('624', '1', 's_ss_snowman', 'Sticker', '39', '0', 'Muñeco de nieve', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('615', '1', 's_ss_bootsitjalapaset_blue', 'Sticker', '39', '0', 'Botas y guantes', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('619', '1', 's_ss_costume_red', 'Sticker', '39', '0', 'Traje, botas y guantes', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('623', '1', 's_ss_snowflake2', 'Sticker', '39', '0', 'Estrella nevada', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('625', '1', 's_ss_snowqueen', 'Sticker', '39', '0', 'Muñeca de nieve', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('62', '1', 's_battle1', 'Sticker', '40', '0', 'Batalla 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('63', '1', 's_battle3', 'Sticker', '40', '0', 'Batalla 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('566', '1', 's_eyeleft', 'Sticker', '42', '0', 'Ojo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('567', '1', 's_eyeright', 'Sticker', '42', '0', 'Ojo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('68', '1', 's_sticker_gurubeard_gray', 'Sticker', '42', '0', 'Barba grisácea', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('69', '1', 's_sticker_gurubeard_brown', 'Sticker', '42', '0', 'Barba amarronada', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('70', '1', 's_sticker_glasses_supernerd', 'Sticker', '42', '0', 'De científico loco', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('71', '1', 's_sticker_glasses_elton', 'Sticker', '42', '0', 'Gafas molonas', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('72', '1', 's_sticker_eyes_blue', 'Sticker', '42', '0', 'Azul...', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('73', '1', 's_sticker_eye_anim', 'Sticker', '42', '0', 'Ojo animado', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('74', '1', 's_sticker_eye_evil_anim', 'Sticker', '42', '0', 'Ojo endiablado', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('694', '1', 's_sticker_eraser', 'Sticker', '44', '0', 'Borrador', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('713', '1', 's_star', 'Sticker', '44', '0', 'Estrella', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('711', '1', 's_sticker_pencil', 'Sticker', '44', '0', 'Lápiz', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('629', '1', 's_sticker_dreamer', 'Sticker', '44', '0', 'Soñador', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('712', '1', 's_sticker_pencil_2', 'Sticker', '44', '0', 'Lápiz', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('631', '1', 's_sticker_lonewolf', 'Sticker', '44', '0', 'Lobo Solitario', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('634', '1', 's_sticker_prankster', 'Sticker', '44', '0', 'Bromista', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('635', '1', 's_sticker_romantic', 'Sticker', '44', '0', 'Romántico', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('15', '1', 's_redlamp', 'Sticker', '44', '0', 'Lámpara Roja', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('14', '1', 's_lightbulb', 'Sticker', '44', '0', 'Bombilla', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('26', '1', 's_bullet1', 'Sticker', '44', '0', 'Agujeros de bala', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('27', '1', 's_spill1', 'Sticker', '44', '0', 'Derramado 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('28', '1', 's_spill2', 'Sticker', '44', '0', 'Derramado 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('29', '1', 's_spill3', 'Sticker', '44', '0', 'Derramado 3', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('30', '1', 's_sticker_coffee_stain', 'Sticker', '44', '0', 'Mancha de café', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('31', '1', 's_sticker_hole', 'Sticker', '44', '0', 'Agujero', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('32', '1', 's_sticker_flames', 'Sticker', '44', '0', 'Llamas', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('33', '1', 's_paper_clip_1', 'Sticker', '44', '0', 'mypage.sticker.paper_clip_1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('34', '1', 's_paper_clip_2', 'Sticker', '44', '0', 'Clip 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('35', '1', 's_paper_clip_3', 'Sticker', '44', '0', 'Clip3', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('41', '1', 's_highlighter_1', 'Sticker', '44', '0', 'Lo más 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('42', '1', 's_highlighter_mark5', 'Sticker', '44', '0', 'Lo más', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('43', '1', 's_highlighter_mark6', 'Sticker', '44', '0', 'Lo más', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('44', '1', 's_highlighter_mark4b', 'Sticker', '44', '0', 'Lo más', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('45', '1', 's_highlighter_2', 'Sticker', '44', '0', 'Lo más 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('46', '1', 's_highlighter_mark1', 'Sticker', '44', '0', 'Lo más', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('47', '1', 's_highlighter_mark2', 'Sticker', '44', '0', 'Lo más', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('48', '1', 's_highlighter_mark3', 'Sticker', '44', '0', 'Lo más', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('51', '1', 's_plaster', 'Sticker', '44', '0', 'Tirita', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('52', '1', 's_plaster2', 'Sticker', '44', '0', 'Tirita', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('57', '1', 's_sticker_croco', 'Sticker', '44', '0', 'cocodrilo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('60', '1', 's_fish', 'Sticker', '44', '0', 'Pescado', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('61', '1', 's_parrot', 'Sticker', '44', '0', 'Loro', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('64', '1', 's_sticker_sleeping_habbo', 'Sticker', '44', '0', 'Habbo en sueños', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('75', '1', 's_burger', 'Sticker', '44', '0', 'Hamburguesa', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('76', '1', 's_juice', 'Sticker', '44', '0', 'Zumo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('78', '1', 's_sticker_coffee_steam_blue', 'Sticker', '44', '0', 'Mancha de café azul', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('79', '1', 's_sticker_coffee_steam_grey', 'Sticker', '44', '0', 'Mancha de café gris', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('80', '1', 's_cassette1', 'Sticker', '44', '0', 'Cassette', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('81', '1', 's_cassette2', 'Sticker', '44', '0', 'Cassette', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('82', '1', 's_cassette3', 'Sticker', '44', '0', 'Cassette', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('83', '1', 's_cassette4', 'Sticker', '44', '0', 'Cassette', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('98', '1', 's_football', 'Sticker', '44', '0', 'Fútbol', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1626', '1', 'b_bg_fondo_foqb', 'Background', '52', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1618', '1', 'b_es_el_internardo_bg_v1', 'Background', '52', '0', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('724', '1', 'b_bg_bobbaheart', 'Background', '52', '0', 'Corazón Bobba', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('725', '1', 'b_bg_rain', 'Background', '52', '0', 'Lluvia', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('726', '1', 'b_bg_serpentine_1', 'Background', '52', '0', 'Serpentina 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('727', '1', 'b_bg_serpentine_2', 'Background', '52', '0', 'Serpentina 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('728', '1', 'b_bg_serpentine_darkblue', 'Background', '52', '0', 'Serpentina Azul', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('729', '1', 'b_bg_serpntine_darkred', 'Background', '52', '0', 'mypage.background.bg_serpntine_darkred.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('140', '1', 'b_bg_denim', 'Background', '52', '0', 'Vaqueros', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('141', '1', 'b_bg_lace', 'Background', '52', '0', 'Encaje', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('142', '1', 'b_bg_stitched', 'Background', '52', '0', 'Bordado', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('143', '1', 'b_bg_wood', 'Background', '52', '0', 'Madera', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('144', '1', 'b_bg_cork', 'Background', '52', '0', 'Corcho', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('145', '1', 'b_bg_stone', 'Background', '52', '0', 'Piedra', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('146', '1', 'b_bg_pattern_bricks', 'Background', '52', '0', 'Ladrillos', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('147', '1', 'b_bg_ruled_paper', 'Background', '52', '0', 'Papel cuadriculado', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('148', '1', 'b_bg_grass', 'Background', '52', '0', 'Hierba', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('149', '1', 'b_bg_hotel', 'Background', '52', '0', 'Nubes', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('150', '1', 'b_bg_bubble', 'Background', '52', '0', 'Burbuja', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('151', '1', 'b_bg_pattern_bobbaskulls1', 'Background', '52', '0', 'Calavera Bobba', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('152', '1', 'b_bg_pattern_space', 'Background', '52', '0', 'Espacio', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('153', '1', 'b_bg_image_submarine', 'Background', '52', '0', 'Submarino', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('154', '1', 'b_bg_metal2', 'Background', '52', '0', 'Metal 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('155', '1', 'b_bg_broken_glass', 'Background', '52', '0', 'Cristal Roto', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('156', '1', 'b_bg_pattern_clouds', 'Background', '52', '0', 'Nubes', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('157', '1', 'b_bg_comic2', 'Background', '52', '0', 'Cómic 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('158', '1', 'b_bg_pattern_floral_01', 'Background', '52', '0', 'Floral 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('159', '1', 'b_bg_pattern_floral_02', 'Background', '52', '0', 'Floral 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('160', '1', 'b_bg_pattern_floral_03', 'Background', '52', '0', 'Floral 3', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('161', '1', 'b_bg_pattern_bulb', 'Background', '52', '0', 'Bombilla', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('162', '1', 'b_bg_pattern_cars', 'Background', '52', '0', 'Coches', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('164', '1', 'b_bg_pattern_plasto', 'Background', '52', '0', 'Plastic', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('165', '1', 'b_bg_pattern_tinyroom', 'Background', '52', '0', 'Habitación minúscula', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('166', '1', 'b_bg_pattern_hearts', 'Background', '52', '0', 'Corazones', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('167', '1', 'b_bg_pattern_abstract1', 'Background', '52', '0', 'Abstracto 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('168', '1', 'b_bg_bathroom_tile', 'Background', '52', '0', 'Azulejo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('169', '1', 'b_bg_pattern_fish', 'Background', '52', '0', 'Peces', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('170', '1', 'b_bg_pattern_deepred', 'Background', '52', '0', 'Profundidad', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('171', '1', 'b_bg_colour_02', 'Background', '52', '0', '2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('172', '1', 'b_bg_colour_03', 'Background', '52', '0', '3', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('173', '1', 'b_bg_colour_04', 'Background', '52', '0', '4', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('174', '1', 'b_bg_colour_05', 'Background', '52', '0', '5', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('175', '1', 'b_bg_colour_06', 'Background', '52', '0', '6', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('176', '1', 'b_bg_colour_07', 'Background', '52', '0', '7', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('177', '1', 'b_bg_colour_09', 'Background', '52', '0', '9', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('178', '1', 'b_bg_colour_10', 'Background', '52', '0', '10', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('179', '1', 'b_bg_colour_11', 'Background', '52', '0', '11', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('180', '1', 'b_bg_colour_13', 'Background', '52', '0', '13', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('181', '1', 'b_bg_colour_14', 'Background', '52', '0', '14', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('182', '1', 'b_bg_colour_15', 'Background', '52', '0', '15', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('183', '1', 'b_bg_colour_17', 'Background', '52', '0', '17', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1', '1', 's', 'Sticker', '65', '0', 'ss', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1627', '2', 'commodity_stickienote_pre', 'WebCommodity', '54', '3', 'Notas', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1628', '0', 's_circus_sticker_tragafuegos_001', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1629', '0', 's_sticker_checkeredflagr', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1630', '0', 's_stick_country_cow2', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1631', '0', 's_stick_easter_char3', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1632', '0', 's_superheroes_sticker_capa_me', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1633', '0', 's_circus_sticker_trapecista_001', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1634', '0', 's_fhilip', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1635', '0', 's_au_sticker_habbofilmawards_v2', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1636', '0', 's_stick_easter_treeboy', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1637', '0', 's_sasquatch7', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1638', '0', 's_sh_sticker_redbolo_flipped', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1639', '0', 's_stick_easter_char1', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1640', '0', 's_stick_twil_ww', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1641', '0', 's_circus_sticker_malabarista_001', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1642', '0', 's_habbo_stickers', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1643', '0', 's_sticker_es_greusa_2', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1644', '0', 's_sticker_cheetos_hotdog', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1645', '0', 's_sticker_checkerhoriz', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1646', '0', 's_au_sticker_habbofilmawards_v1', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1647', '0', 's_sticker_checkervert', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1648', '0', 's_hw_sticker1_50000', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1649', '0', 's_sticker_littlecandle', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1650', '0', 's_xmas2009_bauble', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1651', '0', 's_xmas2009_reindeerpin', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1652', '0', 's_xmas2009_snowshake', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1653', '0', 's_xmas2009_bubbledrink', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1654', '0', 's_xmas2009_cakes', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1655', '0', 's_sticker_despme_3', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1656', '0', 's_stick_telepizza_scooter', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1657', '0', 's_sticker_straypixels_1012a', 'Sticker', '1', '1', 'Poster by Dragon.Tamer', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1658', '0', 's_sticker_straypixels_1012b', 'Sticker', '1', '1', 'Poster by Shutdown!', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1659', '0', 's_sticker_straypixels_1012c', 'Sticker', '1', '1', 'Poster by Mechoulam', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1660', '0', 's_sticker_straypixels_1012d', 'Sticker', '1', '1', 'Poster by HankMcCoy', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1661', '0', 's_sticker_straypixels_1012e', 'Sticker', '1', '1', 'Poster by ,CrystalBailey', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1662', '0', 's_sticker_straypixels_1012f', 'Sticker', '1', '1', 'Poster by RollerKingdom', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1663', '0', 's_sticker_straypixels_1012g', 'Sticker', '1', '1', 'Poster by Rykz', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1664', '0', 's_sticker_straypixels_1012h', 'Sticker', '1', '1', 'Poster by Mr.Flicker.', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1665', '0', 's_sticker_straypixels_1012i', 'Sticker', '1', '1', 'Poster by avilaman', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1666', '0', 's_hw_amp_big', 'Sticker', '1', '1', 'mypage.sticker.hw_amp_big.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1667', '0', 's_hw_amp_small', 'Sticker', '1', '1', 'mypage.sticker.hw_amp_small.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1668', '0', 's_hw_bassplayer_boy', 'Sticker', '1', '1', 'mypage.sticker.hw_bassplayer_boy.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1669', '0', 's_hw_bassplayer_girl2', 'Sticker', '1', '1', 'mypage.sticker.hw_bassplayer_girl2.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1670', '0', 's_hw_drummer_boy', 'Sticker', '1', '1', 'mypage.sticker.hw_drummer_boy.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1671', '0', 's_hw_drummer_girl', 'Sticker', '1', '1', 'mypage.sticker.hw_drummer_girl.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1672', '0', 's_hw_guitarplayer_black', 'Sticker', '1', '1', 'mypage.sticker.hw_guitarplayer_black.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1673', '0', 's_hw_guitarplayer_white', 'Sticker', '1', '1', 'mypage.sticker.hw_guitarplayer_white.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1674', '0', 's_hw_keyboards2', 'Sticker', '1', '1', 'mypage.sticker.hw_keyboards2.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1675', '0', 's_hw_microphone', 'Sticker', '1', '1', 'mypage.sticker.hw_microphone.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1676', '0', 's_sticker_spiderlove', 'Sticker', '1', '2', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1677', '0', 's_sticker_spiderchomp', 'Sticker', '1', '5', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1678', '0', 's_sticker_circlehearts', 'Sticker', '1', '3', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1679', '0', 's_brownies_sticker', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1680', '0', 's_sunflower', 'Sticker', '1', '1', 'Sunflower', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1681', '0', 's_sticker_soap', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1682', '0', 's_easter_bird', 'Sticker', '1', '1', 'Easter Bird', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1683', '0', 's_easter_broomstick_001', 'Sticker', '1', '1', 'Broomstick', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1684', '0', 's_easter_carrot_rocket', 'Sticker', '1', '1', 'Carrot Rocket', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1685', '0', 's_gorillahand1', 'Sticker', '1', '1', 'Gorilla hand', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1686', '0', 's_gorillahand2', 'Sticker', '1', '1', 'Gorilla hand 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1687', '0', 's_grass', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1688', '0', 's_clothesline', 'Sticker', '1', '1', 'Clothes line', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1689', '0', 's_nl_cupido', 'Sticker', '1', '1', 'mypage.sticker.nl_cupido.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1690', '0', 's_nl_limo', 'Sticker', '1', '1', 'mypage.sticker.nl_limo.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1691', '0', 's_shell', 'Sticker', '1', '1', 'Shell', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1692', '0', 's_summer_blueberry_left', 'Sticker', '1', '1', 'Blueberry left', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1693', '0', 's_summer_blueberry_right', 'Sticker', '1', '1', 'Blueberry right', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1694', '0', 's_summer_kite', 'Sticker', '1', '1', 'Kite', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1695', '0', 's_flashign_led', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1696', '0', 's_bullybusters', 'Sticker', '1', '1', 'BullyBuster', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1697', '0', 's_thanksgiving08_turkey', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1698', '0', 's_sticker_caballoons', 'Sticker', '1', '1', 'mypage.sticker.sticker_caballoons.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1699', '0', 's_bartender_costume', 'Sticker', '1', '1', 'mypage.sticker.bartender_costume.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1700', '0', 's_sg_5thbday_sticker_hat_v1', 'Sticker', '1', '1', 'mypage.sticker.sg_5thbday_sticker_hat_v1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1701', '0', 's_au_bday_greengoldhat_v1', 'Sticker', '1', '1', 'mypage.sticker.au_bday_greengoldhat_v1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1702', '0', 's_sticker_bunnysuit', 'Sticker', '1', '1', 'mypage.sticker.sticker_bunnysuit.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1703', '0', 's_ofw_can_can', 'Sticker', '1', '1', 'mypage.sticker.ofw_can_can.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1704', '0', 's_sticker_cape', 'Sticker', '1', '1', 'Cape', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1705', '0', 's_cheesesuit', 'Sticker', '1', '1', 'Cheese Suit', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1706', '0', 's_claws_lostc_twitchyanim', 'Sticker', '1', '1', 'Crab Claws', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1707', '0', 's_ofw_cuatrero', 'Sticker', '1', '1', 'mypage.sticker.ofw_cuatrero.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1708', '0', 's_crew_male_costume', 'Sticker', '1', '1', 'mypage.sticker.crew_male_costume.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1709', '0', 's_sticker_rcdriverf', 'Sticker', '1', '1', 'Female Racecar Driver', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1710', '0', 's_femalecaptain_costume', 'Sticker', '1', '1', 'mypage.sticker.femalecaptain_costume.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1711', '0', 's_ofw_gorro_indio', 'Sticker', '1', '1', 'mypage.sticker.ofw_gorro_indio.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1712', '0', 's_sticker_rcdriverm1', 'Sticker', '1', '1', 'mypage.sticker.sticker_rcdriverm1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1713', '0', 's_malecaptain_costume', 'Sticker', '1', '1', 'mypage.sticker.malecaptain_costume.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1714', '0', 's_sticker_olym_lady', 'Sticker', '1', '1', 'mypage.sticker.sticker_olym_lady.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1715', '0', 's_sasquatch_hands', 'Sticker', '1', '1', 'mypage.sticker.sasquatch_hands.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1716', '0', 's_sticker_olym_spartan', 'Sticker', '1', '1', 'mypage.sticker.sticker_olym_spartan.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1717', '0', 's_stewardess_costume', 'Sticker', '1', '1', 'mypage.sticker.stewardess_costume.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1718', '0', 's_sticker_arghook', 'Sticker', '1', '1', 'mypage.sticker.sticker_arghook.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1719', '0', 's_sticker_piratehat_1', 'Sticker', '1', '1', 'mypage.sticker.sticker_piratehat_1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1720', '0', 's_sticker_piratehat_2', 'Sticker', '1', '1', 'mypage.sticker.sticker_piratehat_2.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1721', '0', 's_sticker_pumpkinsuit', 'Sticker', '1', '1', 'mypage.sticker.sticker_pumpkinsuit.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1722', '0', 's_sticker_squelette', 'Sticker', '1', '1', 'mypage.sticker.sticker_squelette .title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1723', '0', 's_sticker_turkeysuit', 'Sticker', '1', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1724', '0', 's_habwrecked_sticker_10_45x90', 'Sticker', '1', '1', 'mypage.sticker.habwrecked_sticker_10_45x90.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1725', '0', 's_habwrecked_sticker_11_45x90', 'Sticker', '1', '1', 'mypage.sticker.habwrecked_sticker_11_45x90.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1726', '0', 's_habwrecked_sticker_09_45x90', 'Sticker', '1', '1', 'mypage.sticker.habwrecked_sticker_09_45x90.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1727', '0', 's_habwrecked_sticker_08_45x90', 'Sticker', '1', '1', 'mypage.sticker.habwrecked_sticker_08_45x90.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1728', '0', 's_easteregg_costume', 'Sticker', '2', '1', 'Egg Costume', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1729', '0', 's_beachbunny_bunny_suit', 'Sticker', '2', '1', 'Bunny Suit', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1730', '0', 's_angelwings_anim', 'Sticker', '2', '1', 'Angel Wings', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1731', '0', 's_sticker_bonsai_ninjaf', 'Sticker', '2', '1', 'Female Ninja', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1732', '0', 's_sticker_bonsai_ninjam', 'Sticker', '2', '1', 'Male Ninja', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1733', '0', 's_sticker_bonsai_samuraif', 'Sticker', '2', '1', 'Female Samurai', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1734', '0', 's_sticker_bonsai_samuraim', 'Sticker', '2', '1', 'Male Samurai', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1735', '0', 's_ss_bootsitjalapaset_red', 'Sticker', '2', '1', 'Red Boots', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1736', '0', 's_easter_birdsuit', 'Sticker', '2', '1', 'Bird Suit', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1737', '0', 's_easter_bunnymoped', 'Sticker', '2', '1', 'Bunny Moped', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1738', '0', 's_gothic_draculacape', 'Sticker', '2', '1', 'Dracula Cape', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1739', '0', 's_gothic_scienceplatform', 'Sticker', '2', '1', 'Science Platform', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1740', '0', 's_inked_antisquidf', 'Sticker', '2', '1', 'Anti Squid Female Uniform', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1741', '0', 's_inked_antisquidm', 'Sticker', '2', '1', 'Anti Squid Male Uniform', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1742', '0', 's_inked_inkedblink', 'Sticker', '2', '1', 'Inked Blink', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1743', '0', 's_inked_squidpants', 'Sticker', '2', '1', 'Squid Pants', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1744', '0', 's_july408_auntsamantha', 'Sticker', '2', '1', 'Aunt Samantha', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1745', '0', 's_july408_unclesam', 'Sticker', '2', '1', 'Uncle Sam', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1746', '0', 's_olym_lady', 'Sticker', '2', '1', 'Olympic Lady', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1747', '0', 's_olym_spartan', 'Sticker', '2', '1', 'Olympic Spartan', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1748', '0', 's_promofthedead_sticker_dress', 'Sticker', '2', '1', 'Zombie Dress', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1749', '0', 's_promofthedead_sticker_suit', 'Sticker', '2', '1', 'Zombie Suit', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1750', '0', 's_ballnchain', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1751', '0', 's_summer_swim_trunk', 'Sticker', '2', '1', 'Swimming Floater', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1752', '0', 's_sticker_signfont_a', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1753', '0', 's_sticker_signfont_b', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1754', '0', 's_sticker_signfont_c', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1755', '0', 's_sticker_signfont_d', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1756', '0', 's_sticker_signfont_e', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1757', '0', 's_sticker_signfont_f', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1758', '0', 's_sticker_signfont_g', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1759', '0', 's_sticker_signfont_h', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1760', '0', 's_sticker_signfont_i', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1761', '0', 's_sticker_signfont_j', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1762', '0', 's_sticker_signfont_k', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1763', '0', 's_sticker_signfont_l', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1764', '0', 's_sticker_signfont_m', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1765', '0', 's_sticker_signfont_n', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1766', '0', 's_sticker_signfont_o', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1767', '0', 's_sticker_signfont_p', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1768', '0', 's_sticker_signfont_q', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1769', '0', 's_sticker_signfont_r', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1770', '0', 's_sticker_signfont_s', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1771', '0', 's_sticker_signfont_t', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1772', '0', 's_sticker_signfont_u', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1773', '0', 's_sticker_signfont_v', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1774', '0', 's_sticker_signfont_w', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1775', '0', 's_sticker_signfont_x', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1776', '0', 's_sticker_signfont_y', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1777', '0', 's_sticker_signfont_z', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1778', '0', 's_sticker_signfont_qstn', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1779', '0', 's_sticker_signfont_excl', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1780', '0', 's_sticker_signfont_hyphen', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1781', '0', 's_sticker_signfont_comma', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1782', '0', 's_sticker_signfont_period', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1783', '0', 's_neon_blue_0', 'Sticker', '3', '1', 'mypage.sticker.neon_blue_0.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1784', '0', 's_neon_blue_1', 'Sticker', '3', '1', 'mypage.sticker.neon_blue_1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1785', '0', 's_neon_blue_2', 'Sticker', '3', '1', 'mypage.sticker.neon_blue_2.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1786', '0', 's_neon_blue_3', 'Sticker', '3', '1', 'mypage.sticker.neon_blue_3.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1787', '0', 's_neon_blue_4', 'Sticker', '3', '1', 'mypage.sticker.neon_blue_4.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1788', '0', 's_neon_blue_5', 'Sticker', '3', '1', 'mypage.sticker.neon_blue_5.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1789', '0', 's_neon_blue_6', 'Sticker', '3', '1', 'mypage.sticker.neon_blue_6.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1790', '0', 's_neon_blue_7', 'Sticker', '3', '1', 'mypage.sticker.neon_blue_7.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1791', '0', 's_neon_blue_8', 'Sticker', '3', '1', 'mypage.sticker.neon_blue_8.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1792', '0', 's_neon_blue_9', 'Sticker', '3', '1', 'mypage.sticker.neon_blue_9.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1793', '0', 's_neon_blue_a', 'Sticker', '3', '1', 'A', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1794', '0', 's_neon_blue_b', 'Sticker', '3', '1', 'B', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1795', '0', 's_neon_blue_c', 'Sticker', '3', '1', 'C', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1796', '0', 's_neon_blue_d', 'Sticker', '3', '1', 'D', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1797', '0', 's_neon_blue_e', 'Sticker', '3', '1', 'E', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1798', '0', 's_neon_blue_f', 'Sticker', '3', '1', 'F', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1799', '0', 's_neon_blue_g', 'Sticker', '3', '1', 'G', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1800', '0', 's_neon_blue_h', 'Sticker', '3', '1', 'H', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1801', '0', 's_neon_blue_i', 'Sticker', '3', '1', 'I', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1802', '0', 's_neon_blue_j', 'Sticker', '3', '1', 'J', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1803', '0', 's_neon_blue_k', 'Sticker', '3', '1', 'K', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1804', '0', 's_neon_blue_l', 'Sticker', '3', '1', 'L', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1805', '0', 's_neon_blue_m', 'Sticker', '3', '1', 'M', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1806', '0', 's_neon_blue_n', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1807', '0', 's_neon_blue_o', 'Sticker', '3', '1', 'O', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1808', '0', 's_neon_blue_p', 'Sticker', '3', '1', 'P', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1809', '0', 's_neon_blue_q', 'Sticker', '3', '1', 'Q', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1810', '0', 's_neon_blue_r', 'Sticker', '3', '1', 'R', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1811', '0', 's_neon_blue_s', 'Sticker', '3', '1', 'S', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1812', '0', 's_neon_blue_t', 'Sticker', '3', '1', 'T', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1813', '0', 's_neon_blue_u', 'Sticker', '3', '1', 'U', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1814', '0', 's_neon_blue_v', 'Sticker', '3', '1', 'V', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1815', '0', 's_neon_blue_w', 'Sticker', '3', '1', 'W', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1816', '0', 's_neon_blue_x', 'Sticker', '3', '1', 'X', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1817', '0', 's_neon_blue_y', 'Sticker', '3', '1', 'Y', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1818', '0', 's_neon_blue_z', 'Sticker', '3', '1', 'Z', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1819', '0', 's_neon_pink_0', 'Sticker', '3', '1', 'mypage.sticker.neon_pink_0.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1820', '0', 's_neon_pink_1', 'Sticker', '3', '1', 'mypage.sticker.neon_pink_1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1821', '0', 's_neon_pink_2', 'Sticker', '3', '1', 'mypage.sticker.neon_pink_2.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1822', '0', 's_neon_pink_3', 'Sticker', '3', '1', 'mypage.sticker.neon_pink_3.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1823', '0', 's_neon_pink_4', 'Sticker', '3', '1', 'mypage.sticker.neon_pink_4.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1824', '0', 's_neon_pink_5', 'Sticker', '3', '1', 'mypage.sticker.neon_pink_5.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1825', '0', 's_neon_pink_6', 'Sticker', '3', '1', 'mypage.sticker.neon_pink_6.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1826', '0', 's_neon_pink_7', 'Sticker', '3', '1', 'mypage.sticker.neon_pink_7.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1827', '0', 's_neon_pink_8', 'Sticker', '3', '1', 'mypage.sticker.neon_pink_8.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1828', '0', 's_neon_pink_9', 'Sticker', '3', '1', 'mypage.sticker.neon_pink_9.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1829', '0', 's_neon_pink_a', 'Sticker', '3', '1', 'A', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1830', '0', 's_neon_pink_b', 'Sticker', '3', '1', 'B', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1831', '0', 's_neon_pink_c', 'Sticker', '3', '1', 'C', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1832', '0', 's_neon_pink_d', 'Sticker', '3', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1833', '0', 's_neon_pink_e', 'Sticker', '3', '1', 'E', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1834', '0', 's_neon_pink_f', 'Sticker', '3', '1', 'F', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1835', '0', 's_neon_pink_g', 'Sticker', '3', '1', 'G', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1836', '0', 's_neon_pink_h', 'Sticker', '3', '1', 'H', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1837', '0', 's_neon_pink_i', 'Sticker', '3', '1', 'I', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1838', '0', 's_neon_pink_j', 'Sticker', '3', '1', 'J', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1839', '0', 's_neon_pink_k', 'Sticker', '3', '1', 'K', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1840', '0', 's_neon_pink_l', 'Sticker', '3', '1', 'L', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1841', '0', 's_neon_pink_m', 'Sticker', '3', '1', 'M', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1842', '0', 's_neon_pink_n', 'Sticker', '3', '1', 'N', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1843', '0', 's_neon_pink_o', 'Sticker', '3', '1', 'O', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1844', '0', 's_neon_pink_p', 'Sticker', '3', '1', 'P', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1845', '0', 's_neon_pink_q', 'Sticker', '3', '1', 'Q', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1846', '0', 's_neon_pink_r', 'Sticker', '3', '1', 'R', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1847', '0', 's_neon_pink_s', 'Sticker', '3', '1', 'S', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1848', '0', 's_neon_pink_t', 'Sticker', '3', '1', 'T', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1849', '0', 's_neon_pink_u', 'Sticker', '3', '1', 'U', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1850', '0', 's_neon_pink_v', 'Sticker', '3', '1', 'V', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1851', '0', 's_neon_pink_w', 'Sticker', '3', '1', 'W', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1852', '0', 's_neon_pink_x', 'Sticker', '3', '1', 'X', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1853', '0', 's_neon_pink_y', 'Sticker', '3', '1', 'Y', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1854', '0', 's_neon_pink_z', 'Sticker', '3', '1', 'Z', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1855', '0', 's_green_neon_0', 'Sticker', '3', '1', 'mypage.sticker.green_neon_0.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1856', '0', 's_green_neon_1', 'Sticker', '3', '1', 'mypage.sticker.green_neon_1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1857', '0', 's_green_neon_2', 'Sticker', '3', '1', 'mypage.sticker.green_neon_2.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1858', '0', 's_green_neon_3', 'Sticker', '3', '1', 'mypage.sticker.green_neon_3.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1859', '0', 's_green_neon_4', 'Sticker', '3', '1', 'mypage.sticker.green_neon_4.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1860', '0', 's_green_neon_5', 'Sticker', '3', '1', 'mypage.sticker.green_neon_5.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1861', '0', 's_green_neon_6', 'Sticker', '3', '1', 'mypage.sticker.green_neon_6.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1862', '0', 's_green_neon_7', 'Sticker', '3', '1', 'mypage.sticker.green_neon_7.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1863', '0', 's_green_neon_8', 'Sticker', '3', '1', 'mypage.sticker.green_neon_8.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1864', '0', 's_green_neon_9', 'Sticker', '3', '1', 'mypage.sticker.green_neon_9.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1865', '0', 's_green_neon_a', 'Sticker', '3', '1', 'A', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1866', '0', 's_green_neon_b', 'Sticker', '3', '1', 'B', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1867', '0', 's_green_neon_c', 'Sticker', '3', '1', 'Green Neon C', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1868', '0', 's_green_neon_d', 'Sticker', '3', '1', 'D', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1869', '0', 's_green_neon_e', 'Sticker', '3', '1', 'E', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1870', '0', 's_green_neon_f', 'Sticker', '3', '1', 'F', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1871', '0', 's_green_neon_g', 'Sticker', '3', '1', 'Green Neon G', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1872', '0', 's_green_neon_h', 'Sticker', '3', '1', 'H', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1873', '0', 's_green_neon_i', 'Sticker', '3', '1', 'Green Neon I', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1874', '0', 's_green_neon_j', 'Sticker', '3', '1', 'J', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1875', '0', 's_green_neon_k', 'Sticker', '3', '1', 'K', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1876', '0', 's_green_neon_l', 'Sticker', '3', '1', 'L', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1877', '0', 's_green_neon_m', 'Sticker', '3', '1', 'M', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1878', '0', 's_green_neon_n', 'Sticker', '3', '1', 'Green Neon N', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1879', '0', 's_green_neon_o', 'Sticker', '3', '1', 'O', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1880', '0', 's_green_neon_p', 'Sticker', '3', '1', 'P', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1881', '0', 's_green_neon_q', 'Sticker', '3', '1', 'Green Neon Q', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1882', '0', 's_green_neon_r', 'Sticker', '3', '1', 'R', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1883', '0', 's_green_neon_s', 'Sticker', '3', '1', 'S', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1884', '0', 's_green_neon_t', 'Sticker', '3', '1', 'T', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1885', '0', 's_green_neon_u', 'Sticker', '3', '1', 'U', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1886', '0', 's_green_neon_v', 'Sticker', '3', '1', 'V', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1887', '0', 's_green_neon_w', 'Sticker', '3', '1', 'W', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1888', '0', 's_green_neon_x', 'Sticker', '3', '1', 'Green Neon X', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1889', '0', 's_green_neon_y', 'Sticker', '3', '1', 'Y', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1890', '0', 's_green_neon_z', 'Sticker', '3', '1', 'Z', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1891', '0', 's_fwrk_blue', 'Sticker', '2', '1', 'mypage.sticker.fwrk_blue.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1892', '0', 's_sticker_fireworkboom1', 'Sticker', '2', '1', 'mypage.sticker.sticker_fireworkboom1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1893', '0', 's_sticker_fireworkboom2', 'Sticker', '2', '1', 'mypage.sticker.sticker_fireworkboom2.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1894', '0', 's_sticker_fireworkboom3', 'Sticker', '2', '1', 'mypage.sticker.sticker_fireworkboom3.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1895', '0', 's_sticker_fireworkboom4', 'Sticker', '2', '1', 'mypage.sticker.sticker_fireworkboom4.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1896', '0', 's_fwrk_pink', 'Sticker', '2', '1', 'mypage.sticker.fwrk_pink.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1897', '0', 's_fwrk_yellow', 'Sticker', '2', '1', 'mypage.sticker.fwrk_yellow.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1898', '0', 's_sticker_driver_red', 'Sticker', '2', '1', 'SparkPlug Roberts', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1899', '0', 's_sticker_driver_black', 'Sticker', '2', '1', 'Speed Demon', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1900', '0', 's_sticker_h500_4', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1901', '0', 's_sticker_h500_3', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1902', '0', 's_sticker_h500_2', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1903', '0', 's_sticker_h500_1', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1904', '0', 's_sticker_h500_c1', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1905', '0', 's_sticker_h500_c2', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1906', '0', 's_sticker_h500_c3', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1907', '0', 's_sticker_h500_c4', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1908', '0', 's_sticker_greenflag_r', 'Sticker', '2', '1', 'Green Flag right', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1909', '0', 's_sticker_greenflag_l', 'Sticker', '2', '1', 'Green Flag left', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1910', '0', 's_sticker_checkeredflagl', 'Sticker', '2', '1', 'Checkered Flag left', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1911', '0', 's_sticker_rcdriverm', 'Sticker', '2', '1', 'Male Racecar Driver', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1912', '0', 's_sticker_skidsr', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1913', '0', 's_sticker_skidsl', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1914', '0', 's_superhero1', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1915', '0', 's_superhero2', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1916', '0', 's_superhero3', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1917', '0', 's_superhero4', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1918', '0', 's_superhero5', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1919', '0', 's_superhero6', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1920', '0', 's_superhero7', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1921', '0', 's_piratehook', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1922', '0', 's_piratehat', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1923', '0', 's_piratehatblack', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1924', '0', 's_pirates_captain', 'Sticker', '2', '1', 'Captain', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1925', '0', 's_pirates_dude01', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1926', '0', 's_pirates_dude02', 'Sticker', '2', '1', 'Dude', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1927', '0', 's_pirates_cutlass', 'Sticker', '2', '1', 'Cutlass', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1928', '0', 's_pirates_flag', 'Sticker', '2', '1', 'Pirates Flag', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1929', '0', 's_pirates_scroll', 'Sticker', '2', '1', 'Pirates Scroll', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1930', '0', 's_pirates_treasure01', 'Sticker', '2', '1', 'Treasure', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1931', '0', 's_pirates_treasure02', 'Sticker', '2', '1', 'Treasure', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1932', '0', 's_seagull_lf', 'Sticker', '2', '1', 'mypage.sticker.seagull_lf.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1933', '0', 's_seagull_rf', 'Sticker', '2', '1', 'mypage.sticker.seagull_rf.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1934', '0', 's_edito_fisherman_bait', 'Sticker', '2', '1', 'mypage.sticker.edito_fisherman_bait.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1935', '0', 's_flameskull', 'Sticker', '2', '1', 'Flame Skull', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1936', '0', 's_island', 'Sticker', '2', '1', 'mypage.sticker.island.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1937', '0', 's_summer_rowingboat', 'Sticker', '2', '1', 'Rowboat', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1938', '0', 's_tall_ship', 'Sticker', '2', '1', 'Tall Ship', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1939', '0', 's_val_skull360around_anim', 'Sticker', '2', '1', 'Rotating Skull', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1940', '0', 's_dimsims', 'Sticker', '2', '1', 'mypage.sticker.dimsims.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1941', '0', 's_anim_cook', 'Sticker', '2', '1', 'Cook', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1942', '0', 's_sticker_kitchen_v1', 'Sticker', '2', '1', 'mypage.sticker.sticker_kitchen_v1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1943', '0', 's_sticker_plateborder_v1', 'Sticker', '2', '1', 'mypage.sticker.sticker_plateborder_v1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1944', '0', 's_ikura', 'Sticker', '2', '1', 'mypage.sticker.ikura.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1945', '0', 's_kohada', 'Sticker', '2', '1', 'mypage.sticker.kohada.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1946', '0', 's_maguro', 'Sticker', '2', '1', 'mypage.sticker.maguro.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1947', '0', 's_squid', 'Sticker', '2', '1', 'mypage.sticker.squid.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1948', '0', 's_tamago', 'Sticker', '2', '1', 'mypage.sticker.tamago.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1949', '0', 's_uni', 'Sticker', '2', '1', 'mypage.sticker.uni.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1950', '0', 's_stanleycupsticker', 'Sticker', '2', '1', 'mypage.sticker.stanleycupsticker.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1951', '0', 's_ca_hockeygoalie2', 'Sticker', '2', '1', 'mypage.sticker.ca_hockeygoalie2.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1952', '0', 's_hockey_habbo', 'Sticker', '2', '1', 'mypage.sticker.hockey_habbo.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1953', '0', 's_hockeyref', 'Sticker', '2', '1', 'mypage.sticker.hockeyref.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1954', '0', 's_bluehockeystick', 'Sticker', '2', '1', 'mypage.sticker.bluehockeystick.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1955', '0', 's_greenhockeystick', 'Sticker', '2', '1', 'mypage.sticker.greenhockeystick.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1956', '0', 's_redhockeystick', 'Sticker', '2', '1', 'mypage.sticker.redhockeystick.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1957', '0', 's_checker_border_h', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1958', '0', 's_checker_border_v', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1959', '0', 's_goth_border_horizontal', 'Sticker', '2', '1', 'Gothic Border Horizontal', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1960', '0', 's_goth_border_vertical', 'Sticker', '2', '1', 'Gothic Border Vertical', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1961', '0', 's_val_sticker_barbwire_horis', 'Sticker', '2', '1', 'Barbed Wire 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1962', '0', 's_val_sticker_barbwire_vert', 'Sticker', '2', '1', 'Barbed Wire 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1963', '0', 's_val_sticker_rosewire_corner', 'Sticker', '2', '1', 'Roses in Bloom', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1964', '0', 's_val_sticker_rosewire_vert', 'Sticker', '2', '1', 'Barbed Wire 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1965', '0', 's_val_sticker_rosewire_horis', 'Sticker', '2', '1', 'Barbed Wire 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1966', '0', 's_olym_cresthawk', 'Sticker', '2', '1', 'CrestHawcules', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1967', '0', 's_olym_loderse', 'Sticker', '2', '1', 'Loderseus', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1968', '0', 's_olym_smoothcriminal', 'Sticker', '2', '1', 'SmoothnoCriminallo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1969', '0', 's_olym_nme', 'Sticker', '2', '1', 'notMiceElfycus', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1970', '0', 's_olym_carson', 'Sticker', '2', '1', 'CrashtosCarsonios', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1971', '0', 's_olym_jandelee', 'Sticker', '2', '1', 'Jandelea', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1972', '0', 's_olym_squib', 'Sticker', '2', '1', 'Squibena', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1973', '0', 's_olym_moiraine', 'Sticker', '2', '1', 'Moirainite', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1974', '0', 's_olym_inari', 'Sticker', '2', '1', 'Inaryx', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1975', '0', 's_sticker_bonsai_ninjafa', 'Sticker', '2', '1', 'Animated Female Ninja', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1976', '0', 's_sticker_bonsai_ninjama', 'Sticker', '2', '1', 'Animated Male Ninja', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1977', '0', 's_sticker_bonsai_samuraifa', 'Sticker', '2', '1', 'Animated Female Samurai', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1978', '0', 's_sticker_bonsai_samuraima', 'Sticker', '2', '1', 'Animated Male Samurai', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1979', '0', 's_promofthedead_sticker_zombie', 'Sticker', '2', '1', 'Zombie', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1980', '0', 's_hw_hippie', 'Sticker', '2', '1', 'Hippy Habbo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1981', '0', 's_cny_kungfu_dude', 'Sticker', '2', '1', 'Kung Fu Guy', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1982', '0', 's_sticker_mineur', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1983', '0', 's_hw_hairspray', 'Sticker', '2', '1', 'Hairspray Habbo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1984', '0', 's_hw_hitcher', 'Sticker', '2', '1', 'Hitching Habbo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1985', '0', 's_hw_inmate', 'Sticker', '2', '1', 'Habbo Inmate', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1986', '0', 's_hw_kenny_burger', 'Sticker', '2', '1', 'Habeas Corpus', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1987', '0', 's_hw_kenny_fight', 'Sticker', '2', '1', 'Habeus Corpus w/Ketchup', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1988', '0', 's_hw_kenny_shock', 'Sticker', '2', '1', 'Shocking Habbo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1989', '0', 's_hw_speedobunny', 'Sticker', '2', '1', 'Speedo Bunny', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1990', '0', 's_hw_actionstar', 'Sticker', '2', '1', 'Action Hero', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1991', '0', 's_hw_bouncers', 'Sticker', '2', '1', 'Body Guards', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1992', '0', 's_val_sticker_bartender2', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1993', '0', 's_val_sticker_crew', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1994', '0', 's_val_sticker_stewardess2', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1995', '0', 's_nl_football_guest', 'Sticker', '2', '1', 'mypage.sticker.nl_football_guest.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1996', '0', 's_thanksgiving08_sticker', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1997', '0', 's_hwood07_klaffi2', 'Sticker', '2', '1', 'Habbowood Clapper', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('1998', '0', 'package_product_pre', '', '2', '8', 'mypage.product.hw_filmstrip_package.title', '0', 's_filmstrip_corner_botleft,s_filmstrip_corner_botright,s_filmstrip_corner_topleft,s_filmstrip_corner_topright,s_filmstrip_horiz,s_filmstrip_vert');
INSERT INTO `xdrcms_store_items` VALUES ('1999', '0', 's_filmstrip_corner_botleft', 'Sticker', '2', '1', 'Film Bottom Left', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2000', '0', 's_filmstrip_corner_botright', 'Sticker', '2', '1', 'Film Bottom Right', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2001', '0', 's_filmstrip_corner_topleft', 'Sticker', '2', '1', 'Film Top Left', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2002', '0', 's_filmstrip_corner_topright', 'Sticker', '2', '1', 'Film Top Right', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2003', '0', 's_filmstrip_horiz', 'Sticker', '2', '1', 'Film Horizontal', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2004', '0', 's_filmstrip_vert', 'Sticker', '2', '1', 'Film Vertical', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2005', '0', 's_hw_3d_glasses', 'Sticker', '2', '1', '3D Glasses', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2006', '0', 's_hw_bigcamera', 'Sticker', '2', '1', 'Big Camera', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2007', '0', 's_hw_camera_l', 'Sticker', '2', '1', 'Camera Left', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2008', '0', 's_hw_camera_r', 'Sticker', '2', '1', 'Camera Right', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2009', '0', 'package_product_pre', 'Sticker', '2', '8', 'mypage.product.hw_carpet_package.title', '0', 's_hw_carpet_corner_down,s_hw_carpet_corner_up,s_hw_carpet_end_ldown,s_hw_carpet_end_lup,s_hw_carpet_end_rdown,s_hw_carpet_end_rup,s_hw_carpet_l,s_hw_carpet_r');
INSERT INTO `xdrcms_store_items` VALUES ('2010', '0', 's_hw_carpet_corner_down', 'Sticker', '2', '1', 'Red Carpet Corner Down', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2011', '0', 's_hw_carpet_corner_up', 'Sticker', '2', '1', 'Red Carpet Corner Up', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2012', '0', 's_hw_carpet_end_ldown', 'Sticker', '2', '1', 'Red Carpet End L Down', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2013', '0', 's_hw_carpet_end_lup', 'Sticker', '2', '1', 'Red Carpet End L Up', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2014', '0', 's_hw_carpet_end_rdown', 'Sticker', '2', '1', 'Red Carpet End R Down', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2015', '0', 's_hw_carpet_end_rup', 'Sticker', '2', '1', 'Red Carpet End R Down', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2016', '0', 's_hw_carpet_l', 'Sticker', '2', '1', 'Red Carpet L Middle', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2017', '0', 's_hw_carpet_r', 'Sticker', '2', '1', 'Red Carpet R Middle', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2018', '0', 's_hw_logoanim', 'Sticker', '2', '1', 'Habbowood Logo', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2019', '0', 's_hw_mega_afro', 'Sticker', '2', '1', 'Omega Fro', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2020', '0', 's_hw_shades', 'Sticker', '2', '1', 'Habbowood Shades', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2021', '0', 's_limo_back', 'Sticker', '2', '1', 'Limo Back', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2022', '0', 's_limo_doorpiece', 'Sticker', '2', '1', 'Limo Door', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2023', '0', 's_limo_front', 'Sticker', '2', '1', 'Limo Front', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2024', '0', 's_limo_windowpiece', 'Sticker', '2', '1', 'Limo Window', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2025', '0', 's_money_o', 'Sticker', '2', '1', 'Money Horizontal', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2026', '0', 's_money_stash', 'Sticker', '2', '1', 'Money Stash', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2027', '0', 's_money_v', 'Sticker', '2', '1', 'Money Vertical', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2028', '0', 's_spotlight_sticker2_001', 'Sticker', '2', '1', 'Spotlight 1', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2029', '0', 's_spotlight_sticker_002', 'Sticker', '2', '1', 'Spotlight 2', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2030', '0', 's_au_sticker_habbofilmawards_v3', 'Sticker', '2', '1', 'mypage.sticker.au_sticker_habbofilmawards_v3.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2031', '0', 's_wanted_poster', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2032', '0', 's_es_duck_sticker', 'Sticker', '2', '1', '', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2033', '0', 's_sticker_tiki_flamesboard', 'Sticker', '2', '1', 'Flames Surf Board', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2034', '0', 's_tiki_flowersboard', 'Sticker', '2', '1', 'Flower Surf Board', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2035', '0', 's_tiki_greenboard', 'Sticker', '2', '1', 'Green Surf Board', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2036', '0', 's_tiki_woodboard', 'Sticker', '2', '1', 'Wood Long Board', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2037', '0', 's_tiki_cloudtiki_l', 'Sticker', '2', '1', 'Cloud Tiki Left', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2038', '0', 's_tiki_cloudtiki_r', 'Sticker', '2', '1', 'Cloud Tiki Right', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2039', '0', 's_tiki_planttiki_l', 'Sticker', '2', '1', 'Plant Tiki Left', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2040', '0', 's_tiki_planttiki_r', 'Sticker', '2', '1', 'Plant Tiki Right', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2041', '0', 's_tiki_skulltiki_l', 'Sticker', '2', '1', 'Skull Tiki Left', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2042', '0', 's_tiki_skulltiki_r', 'Sticker', '2', '1', 'Skull Tiki Right', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2043', '0', 's_tiki_watertiki_l', 'Sticker', '2', '1', 'Water Tiki Left', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2044', '0', 's_tiki_watertiki_r', 'Sticker', '2', '1', 'Water Tiki Right', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2045', '0', 's_butterfly_01', 'Sticker', '2', '1', 'Butterlfy', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2046', '0', 's_beachbunny_beachball_bouncing', 'Sticker', '2', '1', 'Bouncing Beachball', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2047', '0', 's_beachbunny_peep', 'Sticker', '2', '1', 'Peep', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2048', '0', 's_beachbunny_roaster_bunny', 'Sticker', '2', '1', 'Bunny Roasting Marshmallow', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2049', '0', 's_littledogs_mechahead', 'Sticker', '2', '1', 'Robot Dog Head', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2050', '0', 's_littledogs_littledog', 'Sticker', '2', '1', 'Little Dog', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2051', '0', 's_littledogs_walkingmecha', 'Sticker', '2', '1', 'Robot Dog', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2052', '0', 's_inked_ship', 'Sticker', '2', '1', 'Inked Ship', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2053', '0', 's_inked_spit', 'Sticker', '2', '1', 'Spitting Squid', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2054', '0', 's_inked_suidpatrol', 'Sticker', '2', '1', 'Squid on Patrol', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2055', '0', 's_skeletor_001', 'Sticker', '2', '1', 'Skeletor', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2056', '0', 's_skull2', 'Sticker', '2', '1', 'Skull', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2057', '0', 's_skull', 'Sticker', '2', '1', 'Skull', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2058', '0', 's_stick_country_cow', 'Sticker', '2', '1', 'mypage.sticker.stick_country_cow.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2059', '0', 's_stick_country_horse', 'Sticker', '2', '1', 'mypage.sticker.stick_country_horse .title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2060', '0', 's_stick_country_pig', 'Sticker', '2', '1', 'mypage.sticker.stick_country_pig .title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2061', '0', 's_stick_country_sheep1', 'Sticker', '2', '1', 'mypage.sticker.stick_country_sheep1.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2062', '0', 's_stick_country_sheep2', 'Sticker', '2', '1', 'mypage.sticker.stick_country_sheep2 .title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2063', '0', 's_sticker_country_donkey', 'Sticker', '2', '1', 'mypage.sticker.sticker_country_donkey.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2064', '0', 's_missj', 'Sticker', '2', '1', 'mypage.sticker.missj.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2065', '0', 's_rasta', 'Sticker', '2', '1', 'mypage.sticker.rasta.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2066', '0', 's_sticker_olym_carson', 'Sticker', '2', '1', 'mypage.sticker.sticker_olym_carson.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2067', '0', 's_sticker_olym_cresthawk', 'Sticker', '2', '1', 'mypage.sticker.sticker_olym_cresthawk.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2068', '0', 's_sticker_olym_inari', 'Sticker', '2', '1', 'mypage.sticker.sticker_olym_inari.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2069', '0', 's_sticker_olym_jandelee', 'Sticker', '2', '1', 'mypage.sticker.sticker_olym_jandelee.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2070', '0', 's_sticker_olym_loderse', 'Sticker', '2', '1', 'mypage.sticker.sticker_olym_loderse.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2071', '0', 's_sticker_olym_moiraine', 'Sticker', '2', '1', 'mypage.sticker.sticker_olym_moiraine.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2072', '0', 's_sticker_olym_nme', 'Sticker', '2', '1', 'mypage.sticker.sticker_olym_nme.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2073', '0', 's_sticker_olym_smoothcriminal', 'Sticker', '2', '1', 'mypage.sticker.sticker_olym_smoothcriminal.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2074', '0', 's_sticker_olym_squib', 'Sticker', '2', '1', 'mypage.sticker.sticker_olym_squib.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2075', '0', 's_valcaptain', 'Sticker', '2', '1', 'mypage.sticker.valcaptain.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2076', '0', 's_valentertainer', 'Sticker', '2', '1', 'mypage.sticker.valentertainer.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2077', '0', 's_valsteward', 'Sticker', '2', '1', 'mypage.sticker.valsteward.title', '0', '');
INSERT INTO `xdrcms_store_items` VALUES ('2078', '0', 's_valstewardess', 'Sticker', '2', '1', 'mypage.sticker.valstewardess.title', '0', '');

-- ----------------------------
-- Table structure for xdrcms_updates
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_updates`;
CREATE TABLE `xdrcms_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('0',' 1',' 2','3') DEFAULT '3',
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of xdrcms_updates
-- ----------------------------
INSERT INTO `xdrcms_updates` VALUES ('1', '0', 'Nuevo Diseño \'iJollyness\'', 'iJollyness la sensación de aXDR.', '2018-05-18');
INSERT INTO `xdrcms_updates` VALUES ('2', '0', 'Nuevo Sistema de Rango', 'Se empieza un nuevo sistema de Rangos para la CMS.', '2018-05-18');
INSERT INTO `xdrcms_updates` VALUES ('3', ' 2', '&iexcl;ASE Sensacional!', 'Ahora la All Seeing Eye 0.5 Beta te permitirá realizar movimientos rapidos.', '2018-05-18');
INSERT INTO `xdrcms_updates` VALUES ('4', '3', 'Probando Sistema de Updates', 'Este sistema sera muy util', '2018-05-07');
SET FOREIGN_KEY_CHECKS=1;
