aXDR CMS: README
=============

Getting Started
-------
aXDR CMS is a fresh from scratch Emulation website which emulates HabboWEB.

aXDR stands for Azure XDR Content Management System.

Features
-------
>PHP - Networking

>MySQLi - DB Connection

Team
-------
>Rodolfo Contreras (Mr.Mustache)

Special Thanks to
-------
For initializing the project...

>Boris Akopov (Xdr)

Version
-------
4.0.1