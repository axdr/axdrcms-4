aXDR CMS: Changelog
=============

## 4.0.0 - 2018-05-30
### Añadido
- Pagina de Articulos
- Pagina de Ajustes
- Sistema de Permisos

### Cambiado
- Sistema de ReCaptcha v2
- Sistema de Rangos
- Sistema de Articulos
- Diseño Predeterminado
- Diseño de Edición de Usuarios en ASE

### Removido
- ACP Viejo Sistema